-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema sice
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `sice` ;

-- -----------------------------------------------------
-- Schema sice
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `sice` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
USE `sice` ;

-- -----------------------------------------------------
-- Table `sice`.`Responsaveis`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sice`.`Responsaveis` ;

CREATE TABLE IF NOT EXISTS `sice`.`Responsaveis` (
  `matricula` VARCHAR(10) NOT NULL,
  `cpf` VARCHAR(11) NOT NULL COMMENT 'Apenas números',
  `nome` VARCHAR(50) NOT NULL,
  `sobrenome` VARCHAR(50) NOT NULL,
  `sexo` VARCHAR(10) NOT NULL COMMENT 'Masculino/Feminino',
  `dtNascimento` DATE NOT NULL,
  `email` VARCHAR(200) NOT NULL,
  `dtCadastro` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Auto gerada, registra a data-hora da criação do registro',
  `status` SMALLINT NOT NULL DEFAULT 1 COMMENT 'Status do responsavel. 0-Bloqueado ;1-Ativo',
  `telefone` VARCHAR(20) NULL DEFAULT NULL COMMENT 'Apenas números',
  `endereco` VARCHAR(200) NULL DEFAULT NULL COMMENT 'Rua, av., bairro, apto, bloco, número',
  `cidade` VARCHAR(200) NULL DEFAULT NULL COMMENT 'Cidade onde reside',
  `uf` VARCHAR(2) NULL DEFAULT NULL COMMENT 'Estado onde reside',
  `cep` VARCHAR(8) NULL DEFAULT NULL COMMENT 'CEP da residência',
  PRIMARY KEY (`matricula`),
  UNIQUE INDEX `cpf_UNIQUE` (`cpf` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `sice`.`Alunos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sice`.`Alunos` ;

CREATE TABLE IF NOT EXISTS `sice`.`Alunos` (
  `matricula` VARCHAR(10) NOT NULL,
  `cpf` VARCHAR(11) NOT NULL COMMENT 'Apenas números',
  `nome` VARCHAR(50) NOT NULL,
  `sobrenome` VARCHAR(50) NOT NULL,
  `sexo` VARCHAR(10) NOT NULL COMMENT 'Masculino/Feminino',
  `dtNascimento` DATE NOT NULL,
  `email` VARCHAR(200) NOT NULL,
  `dtCadastro` TIMESTAMP NOT NULL DEFAULT NOW() COMMENT 'Auto gerada, registra a data-hora da criação do registro',
  `temNecessidadeEspecial` TINYINT NOT NULL DEFAULT 0 COMMENT '0-Falso; 1-Verdadeiro',
  `temResponsavel` TINYINT NOT NULL DEFAULT 0 COMMENT '0-Falso; 1-Verdadeiro',
  `status` SMALLINT NOT NULL DEFAULT 1 COMMENT 'Status do aluno. 0-Bloqueado; 1-Ativo ;2-Desligado ;3-Trancado',
  `telefone` VARCHAR(20) NULL COMMENT 'Apenas números',
  `endereco` VARCHAR(200) NULL COMMENT 'Rua, av., bairro, apto, bloco, número',
  `cidade` VARCHAR(200) NULL COMMENT 'Cidade onde reside',
  `uf` VARCHAR(2) NULL COMMENT 'Estado onde reside',
  `cep` VARCHAR(8) NULL COMMENT 'CEP da residência',
  `Responsaveis_matricula` VARCHAR(10) NULL,
  PRIMARY KEY (`matricula`),
  UNIQUE INDEX `cpf_UNIQUE` (`cpf` ASC) VISIBLE,
  INDEX `fk_Alunos_Responsaveis1_idx` (`Responsaveis_matricula` ASC) VISIBLE,
  CONSTRAINT `fk_Alunos_Responsaveis1`
    FOREIGN KEY (`Responsaveis_matricula`)
    REFERENCES `sice`.`Responsaveis` (`matricula`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sice`.`Cargos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sice`.`Cargos` ;

CREATE TABLE IF NOT EXISTS `sice`.`Cargos` (
  `codigo` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(50) NOT NULL,
  `descricao` VARCHAR(255) NULL,
  `competencias` TEXT NULL,
  `pisoSalarial` DOUBLE NULL DEFAULT 0,
  PRIMARY KEY (`codigo`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sice`.`Funcionarios`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sice`.`Funcionarios` ;

CREATE TABLE IF NOT EXISTS `sice`.`Funcionarios` (
  `matricula` VARCHAR(10) NOT NULL,
  `cpf` VARCHAR(11) NOT NULL COMMENT 'Apenas números',
  `nome` VARCHAR(50) NOT NULL,
  `sobrenome` VARCHAR(50) NOT NULL,
  `sexo` VARCHAR(10) NOT NULL COMMENT 'Masculino/Feminino',
  `dtNascimento` DATE NOT NULL,
  `email` VARCHAR(200) NOT NULL,
  `dtCadastro` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Auto gerada, registra a data-hora da criação do registro',
  `status` SMALLINT NOT NULL DEFAULT 1 COMMENT 'Status do funcionario. 0-Bloqueado; 1-Ativo; 2-Desligado; 3-Afastado',
  `telefone` VARCHAR(20) NULL DEFAULT NULL COMMENT 'Apenas números',
  `endereco` VARCHAR(200) NULL DEFAULT NULL COMMENT 'Rua, av., bairro, apto, bloco, número',
  `cidade` VARCHAR(200) NULL DEFAULT NULL COMMENT 'Cidade onde reside',
  `uf` VARCHAR(2) NULL DEFAULT NULL COMMENT 'Estado onde reside',
  `cep` VARCHAR(8) NULL DEFAULT NULL COMMENT 'CEP da residência',
  `cargos_codigo` INT NOT NULL,
  PRIMARY KEY (`matricula`),
  UNIQUE INDEX `cpf_UNIQUE` (`cpf` ASC) VISIBLE,
  INDEX `fk_funcionarios_cargos_idx` (`cargos_codigo` ASC) VISIBLE,
  CONSTRAINT `fk_funcionarios_cargos`
    FOREIGN KEY (`cargos_codigo`)
    REFERENCES `sice`.`Cargos` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `sice`.`Titulacoes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sice`.`Titulacoes` ;

CREATE TABLE IF NOT EXISTS `sice`.`Titulacoes` (
  `codigo` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) NOT NULL COMMENT 'Graduação, Mestre, Doutor, Especialista, etc.',
  `pisoHoraAula` DOUBLE NOT NULL DEFAULT 0 COMMENT 'Valordo piso salarial para a hora-aula do titulo',
  PRIMARY KEY (`codigo`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sice`.`Professores`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sice`.`Professores` ;

CREATE TABLE IF NOT EXISTS `sice`.`Professores` (
  `matricula` VARCHAR(10) NOT NULL,
  `cpf` VARCHAR(11) NOT NULL COMMENT 'Apenas números',
  `nome` VARCHAR(50) NOT NULL,
  `sobrenome` VARCHAR(50) NOT NULL,
  `sexo` VARCHAR(10) NOT NULL COMMENT 'Masculino/Feminino',
  `dtNascimento` DATE NOT NULL,
  `email` VARCHAR(200) NOT NULL,
  `dtCadastro` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Auto gerada, registra a data-hora da criação do registro',
  `status` SMALLINT NOT NULL DEFAULT 1 COMMENT 'Status do professor. 0-Bloqueado; 1-Ativo ;2-Desligado; 3-Afastado',
  `telefone` VARCHAR(20) NULL DEFAULT NULL COMMENT 'Apenas números',
  `endereco` VARCHAR(200) NULL DEFAULT NULL COMMENT 'Rua, av., bairro, apto, bloco, número',
  `cidade` VARCHAR(200) NULL DEFAULT NULL COMMENT 'Cidade onde reside',
  `uf` VARCHAR(2) NULL DEFAULT NULL COMMENT 'Estado onde reside',
  `cep` VARCHAR(8) NULL DEFAULT NULL COMMENT 'CEP da residência',
  `valorHoraAula` DOUBLE NULL DEFAULT 0,
  `Titulacao_codigo` INT NOT NULL,
  PRIMARY KEY (`matricula`),
  UNIQUE INDEX `cpf_UNIQUE` (`cpf` ASC) VISIBLE,
  INDEX `fk_professores_Titulacao1_idx` (`Titulacao_codigo` ASC) VISIBLE,
  CONSTRAINT `fk_professores_Titulacao1`
    FOREIGN KEY (`Titulacao_codigo`)
    REFERENCES `sice`.`Titulacoes` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `sice`.`NecessidadesEspeciais`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sice`.`NecessidadesEspeciais` ;

CREATE TABLE IF NOT EXISTS `sice`.`NecessidadesEspeciais` (
  `codigo` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) NOT NULL,
  `descricao` VARCHAR(255) NOT NULL,
  `requisitos` TEXT NULL COMMENT 'Requisitos especiais para atender a necessidade do aluno. Intérprete, tradutor, teclado especial, sala especial, etc.',
  PRIMARY KEY (`codigo`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sice`.`AlunoNecessidade`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sice`.`AlunoNecessidade` ;

CREATE TABLE IF NOT EXISTS `sice`.`AlunoNecessidade` (
  `NecessidadesEspeciais_codigo` INT NOT NULL,
  `Alunos_matricula` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`NecessidadesEspeciais_codigo`, `Alunos_matricula`),
  INDEX `fk_AlunoNecessidade_Alunos1_idx` (`Alunos_matricula` ASC) VISIBLE,
  CONSTRAINT `fk_AlunoNecessidade_NecessidadesEspeciais1`
    FOREIGN KEY (`NecessidadesEspeciais_codigo`)
    REFERENCES `sice`.`NecessidadesEspeciais` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_AlunoNecessidade_Alunos1`
    FOREIGN KEY (`Alunos_matricula`)
    REFERENCES `sice`.`Alunos` (`matricula`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sice`.`SalaTipos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sice`.`SalaTipos` ;

CREATE TABLE IF NOT EXISTS `sice`.`SalaTipos` (
  `codigo` INT NOT NULL AUTO_INCREMENT,
  `descricao` VARCHAR(255) NOT NULL,
  `status` SMALLINT NOT NULL DEFAULT 1 COMMENT '0-Inativo; 1-Ativo',
  PRIMARY KEY (`codigo`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sice`.`Salas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sice`.`Salas` ;

CREATE TABLE IF NOT EXISTS `sice`.`Salas` (
  `codigo` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) NOT NULL,
  `status` SMALLINT NOT NULL DEFAULT 1 COMMENT '0-Inativo; 1-Ativo; 2-Em manutenção; 3-Indisponível',
  `SalaTipos_codigo` INT NOT NULL,
  PRIMARY KEY (`codigo`),
  INDEX `fk_Salas_SalaTipos1_idx` (`SalaTipos_codigo` ASC) VISIBLE,
  CONSTRAINT `fk_Salas_SalaTipos1`
    FOREIGN KEY (`SalaTipos_codigo`)
    REFERENCES `sice`.`SalaTipos` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sice`.`Equipamentos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sice`.`Equipamentos` ;

CREATE TABLE IF NOT EXISTS `sice`.`Equipamentos` (
  `codigo` INT NOT NULL AUTO_INCREMENT,
  `numeroOrdem` VARCHAR(30) NOT NULL,
  `nome` VARCHAR(45) NOT NULL,
  `descricao` VARCHAR(45) NULL,
  `marca` VARCHAR(45) NULL,
  `dtAquisicao` DATE NULL,
  `valorCompra` DOUBLE NULL DEFAULT 0,
  `status` SMALLINT NOT NULL DEFAULT 1 COMMENT '0-Inativo; 1-Ativo; 2-Em manutenção; 3-Indisponível',
  `Salas_codigo` INT NOT NULL,
  PRIMARY KEY (`codigo`),
  INDEX `fk_Equipamentos_Salas1_idx` (`Salas_codigo` ASC) VISIBLE,
  CONSTRAINT `fk_Equipamentos_Salas1`
    FOREIGN KEY (`Salas_codigo`)
    REFERENCES `sice`.`Salas` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sice`.`InsumoCategorias`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sice`.`InsumoCategorias` ;

CREATE TABLE IF NOT EXISTS `sice`.`InsumoCategorias` (
  `codigo` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) NOT NULL,
  `status` SMALLINT NOT NULL DEFAULT 1 COMMENT '0-Inativo; 1-Ativo',
  PRIMARY KEY (`codigo`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sice`.`Insumos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sice`.`Insumos` ;

CREATE TABLE IF NOT EXISTS `sice`.`Insumos` (
  `codigo` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NOT NULL,
  `descricao` VARCHAR(45) NOT NULL,
  `status` SMALLINT NOT NULL DEFAULT 1 COMMENT '0-Inativo; 1-Ativo',
  `qtdeMinima` DOUBLE NOT NULL DEFAULT 0,
  `qtdeMaxima` DOUBLE NOT NULL DEFAULT 0,
  `qtdeAtual` DOUBLE NOT NULL DEFAULT 0,
  `InsumoCategorias_codigo` INT NOT NULL,
  PRIMARY KEY (`codigo`),
  INDEX `fk_Insumos_InsumoCategorias1_idx` (`InsumoCategorias_codigo` ASC) VISIBLE,
  CONSTRAINT `fk_Insumos_InsumoCategorias1`
    FOREIGN KEY (`InsumoCategorias_codigo`)
    REFERENCES `sice`.`InsumoCategorias` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sice`.`Fornecedores`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sice`.`Fornecedores` ;

CREATE TABLE IF NOT EXISTS `sice`.`Fornecedores` (
  `codigo` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) NOT NULL,
  `status` SMALLINT NOT NULL DEFAULT 1 COMMENT '0-Inativo; 1-Ativo',
  PRIMARY KEY (`codigo`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sice`.`FornecedorInsumo`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sice`.`FornecedorInsumo` ;

CREATE TABLE IF NOT EXISTS `sice`.`FornecedorInsumo` (
  `Fornecedores_codigo` INT NOT NULL,
  `Insumos_codigo` INT NOT NULL,
  PRIMARY KEY (`Fornecedores_codigo`, `Insumos_codigo`),
  INDEX `fk_FornecedorInsumo_Insumos1_idx` (`Insumos_codigo` ASC) VISIBLE,
  CONSTRAINT `fk_FornecedorInsumo_Fornecedores1`
    FOREIGN KEY (`Fornecedores_codigo`)
    REFERENCES `sice`.`Fornecedores` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_FornecedorInsumo_Insumos1`
    FOREIGN KEY (`Insumos_codigo`)
    REFERENCES `sice`.`Insumos` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sice`.`CalendariosLetivos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sice`.`CalendariosLetivos` ;

CREATE TABLE IF NOT EXISTS `sice`.`CalendariosLetivos` (
  `codigo` INT NOT NULL AUTO_INCREMENT,
  `ano` SMALLINT NOT NULL COMMENT 'Ano letivo',
  `dtInicio` VARCHAR(45) NOT NULL COMMENT 'Data incial do evento',
  `dtFim` VARCHAR(45) NOT NULL COMMENT 'Data final do evento',
  `evento` TEXT NOT NULL COMMENT 'Descricao do evento',
  PRIMARY KEY (`codigo`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sice`.`Disciplinas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sice`.`Disciplinas` ;

CREATE TABLE IF NOT EXISTS `sice`.`Disciplinas` (
  `codigo` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(255) NOT NULL,
  `status` SMALLINT NOT NULL DEFAULT 1 COMMENT '0-Inativo; 1-Ativo',
  `descricao` TEXT NULL,
  `ementa` TEXT NULL,
  `bibliografia` TEXT NULL,
  `cargaHoraria` DOUBLE NULL,
  PRIMARY KEY (`codigo`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sice`.`ProfessorDisciplina`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sice`.`ProfessorDisciplina` ;

CREATE TABLE IF NOT EXISTS `sice`.`ProfessorDisciplina` (
  `Professores_matricula` VARCHAR(10) NOT NULL,
  `Disciplinas_codigo` INT NOT NULL,
  PRIMARY KEY (`Professores_matricula`, `Disciplinas_codigo`),
  INDEX `fk_ProfessorDisciplina_Disciplinas1_idx` (`Disciplinas_codigo` ASC) VISIBLE,
  CONSTRAINT `fk_ProfessorDisciplina_Professores1`
    FOREIGN KEY (`Professores_matricula`)
    REFERENCES `sice`.`Professores` (`matricula`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ProfessorDisciplina_Disciplinas1`
    FOREIGN KEY (`Disciplinas_codigo`)
    REFERENCES `sice`.`Disciplinas` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sice`.`Cursos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sice`.`Cursos` ;

CREATE TABLE IF NOT EXISTS `sice`.`Cursos` (
  `codigo` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(255) NOT NULL,
  `descricao` TEXT NOT NULL,
  `status` SMALLINT NOT NULL DEFAULT 1 COMMENT '0-Inativo; 1-Ativo',
  `qtdeMinimaAlunos` INT NOT NULL DEFAULT 0,
  `qtdeMaximaAlunos` INT NOT NULL DEFAULT 0,
  `valorCusto` DOUBLE NOT NULL DEFAULT 0,
  `valorVenda` DOUBLE NOT NULL DEFAULT 0,
  PRIMARY KEY (`codigo`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sice`.`CursoDisciplina`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sice`.`CursoDisciplina` ;

CREATE TABLE IF NOT EXISTS `sice`.`CursoDisciplina` (
  `Disciplinas_codigo` INT NOT NULL,
  `Cursos_codigo` INT NOT NULL,
  PRIMARY KEY (`Disciplinas_codigo`, `Cursos_codigo`),
  INDEX `fk_CursoDisciplina_Cursos1_idx` (`Cursos_codigo` ASC) VISIBLE,
  CONSTRAINT `fk_CursoDisciplina_Disciplinas1`
    FOREIGN KEY (`Disciplinas_codigo`)
    REFERENCES `sice`.`Disciplinas` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_CursoDisciplina_Cursos1`
    FOREIGN KEY (`Cursos_codigo`)
    REFERENCES `sice`.`Cursos` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sice`.`Turmas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sice`.`Turmas` ;

CREATE TABLE IF NOT EXISTS `sice`.`Turmas` (
  `codigo` VARCHAR(10) NOT NULL,
  `nome` VARCHAR(45) NOT NULL,
  `status` SMALLINT NOT NULL DEFAULT 1 COMMENT '0-Encerrada; 1-Ativo; 2-Confirmada; 3-Em formação; 4-Cancelada; 5-Suspensa',
  `dtInicio` DATE NOT NULL,
  `dtFim` DATE NOT NULL,
  `turno` VARCHAR(20) NOT NULL,
  `Cursos_codigo` INT NOT NULL,
  PRIMARY KEY (`codigo`),
  INDEX `fk_Turmas_Cursos1_idx` (`Cursos_codigo` ASC) VISIBLE,
  CONSTRAINT `fk_Turmas_Cursos1`
    FOREIGN KEY (`Cursos_codigo`)
    REFERENCES `sice`.`Cursos` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
