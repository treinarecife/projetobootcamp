CREATE DATABASE  IF NOT EXISTS `sice` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `sice`;
-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: localhost    Database: sice
-- ------------------------------------------------------
-- Server version	8.0.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alunonecessidade`
--

DROP TABLE IF EXISTS `alunonecessidade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `alunonecessidade` (
  `NecessidadesEspeciais_codigo` int(11) NOT NULL,
  `Alunos_matricula` varchar(10) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL,
  PRIMARY KEY (`NecessidadesEspeciais_codigo`,`Alunos_matricula`),
  KEY `fk_AlunoNecessidade_Alunos1_idx` (`Alunos_matricula`),
  CONSTRAINT `fk_AlunoNecessidade_Alunos1` FOREIGN KEY (`Alunos_matricula`) REFERENCES `alunos` (`matricula`),
  CONSTRAINT `fk_AlunoNecessidade_NecessidadesEspeciais1` FOREIGN KEY (`NecessidadesEspeciais_codigo`) REFERENCES `necessidadesespeciais` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alunonecessidade`
--

/*!40000 ALTER TABLE `alunonecessidade` DISABLE KEYS */;
/*!40000 ALTER TABLE `alunonecessidade` ENABLE KEYS */;

--
-- Table structure for table `alunos`
--

DROP TABLE IF EXISTS `alunos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `alunos` (
  `matricula` varchar(10) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL,
  `cpf` varchar(11) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL COMMENT 'Apenas números',
  `nome` varchar(50) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL,
  `sobrenome` varchar(50) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL,
  `sexo` varchar(10) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL COMMENT 'Masculino/Feminino',
  `dtNascimento` date NOT NULL,
  `email` varchar(200) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL,
  `dtCadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Auto gerada, registra a data-hora da criação do registro',
  `temNecessidadeEspecial` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0-Falso; 1-Verdadeiro',
  `temResponsavel` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0-Falso; 1-Verdadeiro',
  `status` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Status do aluno. 0-Bloqueado; 1-Ativo ;2-Desligado ;3-Trancado',
  `telefone` varchar(20) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci DEFAULT NULL COMMENT 'Apenas números',
  `endereco` varchar(200) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci DEFAULT NULL COMMENT 'Rua, av., bairro, apto, bloco, número',
  `cidade` varchar(200) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci DEFAULT NULL COMMENT 'Cidade onde reside',
  `uf` varchar(2) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci DEFAULT NULL COMMENT 'Estado onde reside',
  `cep` varchar(8) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci DEFAULT NULL COMMENT 'CEP da residência',
  `Responsaveis_matricula` varchar(10) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci DEFAULT NULL,
  PRIMARY KEY (`matricula`),
  UNIQUE KEY `cpf_UNIQUE` (`cpf`),
  KEY `fk_Alunos_Responsaveis1_idx` (`Responsaveis_matricula`),
  CONSTRAINT `fk_Alunos_Responsaveis1` FOREIGN KEY (`Responsaveis_matricula`) REFERENCES `responsaveis` (`matricula`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alunos`
--

/*!40000 ALTER TABLE `alunos` DISABLE KEYS */;
/*!40000 ALTER TABLE `alunos` ENABLE KEYS */;

--
-- Table structure for table `calendariosletivos`
--

DROP TABLE IF EXISTS `calendariosletivos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `calendariosletivos` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `ano` smallint(6) NOT NULL COMMENT 'Ano letivo',
  `dtInicio` varchar(45) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL COMMENT 'Data incial do evento',
  `dtFim` varchar(45) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL COMMENT 'Data final do evento',
  `evento` text CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL COMMENT 'Descricao do evento',
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calendariosletivos`
--

/*!40000 ALTER TABLE `calendariosletivos` DISABLE KEYS */;
/*!40000 ALTER TABLE `calendariosletivos` ENABLE KEYS */;

--
-- Table structure for table `cargos`
--

DROP TABLE IF EXISTS `cargos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `cargos` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL,
  `descricao` varchar(255) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci DEFAULT NULL,
  `competencias` text CHARACTER SET utf8 COLLATE utf8_0900_ai_ci,
  `pisoSalarial` double DEFAULT '0',
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cargos`
--

/*!40000 ALTER TABLE `cargos` DISABLE KEYS */;
/*!40000 ALTER TABLE `cargos` ENABLE KEYS */;

--
-- Table structure for table `cursodisciplina`
--

DROP TABLE IF EXISTS `cursodisciplina`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `cursodisciplina` (
  `Disciplinas_codigo` int(11) NOT NULL,
  `Cursos_codigo` int(11) NOT NULL,
  PRIMARY KEY (`Disciplinas_codigo`,`Cursos_codigo`),
  KEY `fk_CursoDisciplina_Cursos1_idx` (`Cursos_codigo`),
  CONSTRAINT `fk_CursoDisciplina_Cursos1` FOREIGN KEY (`Cursos_codigo`) REFERENCES `cursos` (`codigo`),
  CONSTRAINT `fk_CursoDisciplina_Disciplinas1` FOREIGN KEY (`Disciplinas_codigo`) REFERENCES `disciplinas` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cursodisciplina`
--

/*!40000 ALTER TABLE `cursodisciplina` DISABLE KEYS */;
/*!40000 ALTER TABLE `cursodisciplina` ENABLE KEYS */;

--
-- Table structure for table `cursos`
--

DROP TABLE IF EXISTS `cursos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `cursos` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL,
  `descricao` text CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '1' COMMENT '0-Inativo; 1-Ativo',
  `qtdeMinimaAlunos` int(11) NOT NULL DEFAULT '0',
  `qtdeMaximaAlunos` int(11) NOT NULL DEFAULT '0',
  `valorCusto` double NOT NULL DEFAULT '0',
  `valorVenda` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cursos`
--

/*!40000 ALTER TABLE `cursos` DISABLE KEYS */;
/*!40000 ALTER TABLE `cursos` ENABLE KEYS */;

--
-- Table structure for table `disciplinas`
--

DROP TABLE IF EXISTS `disciplinas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `disciplinas` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '1' COMMENT '0-Inativo; 1-Ativo',
  `descricao` text CHARACTER SET utf8 COLLATE utf8_0900_ai_ci,
  `ementa` text CHARACTER SET utf8 COLLATE utf8_0900_ai_ci,
  `bibliografia` text CHARACTER SET utf8 COLLATE utf8_0900_ai_ci,
  `cargaHoraria` double DEFAULT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `disciplinas`
--

/*!40000 ALTER TABLE `disciplinas` DISABLE KEYS */;
/*!40000 ALTER TABLE `disciplinas` ENABLE KEYS */;

--
-- Table structure for table `equipamentos`
--

DROP TABLE IF EXISTS `equipamentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `equipamentos` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `numeroOrdem` varchar(30) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL,
  `nome` varchar(45) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL,
  `descricao` varchar(45) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci DEFAULT NULL,
  `marca` varchar(45) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci DEFAULT NULL,
  `dtAquisicao` date DEFAULT NULL,
  `valorCompra` double DEFAULT '0',
  `status` smallint(6) NOT NULL DEFAULT '1' COMMENT '0-Inativo; 1-Ativo; 2-Em manutenção; 3-Indisponível',
  `Salas_codigo` int(11) NOT NULL,
  PRIMARY KEY (`codigo`),
  KEY `fk_Equipamentos_Salas1_idx` (`Salas_codigo`),
  CONSTRAINT `fk_Equipamentos_Salas1` FOREIGN KEY (`Salas_codigo`) REFERENCES `salas` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipamentos`
--

/*!40000 ALTER TABLE `equipamentos` DISABLE KEYS */;
/*!40000 ALTER TABLE `equipamentos` ENABLE KEYS */;

--
-- Table structure for table `fornecedores`
--

DROP TABLE IF EXISTS `fornecedores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `fornecedores` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '1' COMMENT '0-Inativo; 1-Ativo',
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fornecedores`
--

/*!40000 ALTER TABLE `fornecedores` DISABLE KEYS */;
/*!40000 ALTER TABLE `fornecedores` ENABLE KEYS */;

--
-- Table structure for table `fornecedorinsumo`
--

DROP TABLE IF EXISTS `fornecedorinsumo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `fornecedorinsumo` (
  `Fornecedores_codigo` int(11) NOT NULL,
  `Insumos_codigo` int(11) NOT NULL,
  PRIMARY KEY (`Fornecedores_codigo`,`Insumos_codigo`),
  KEY `fk_FornecedorInsumo_Insumos1_idx` (`Insumos_codigo`),
  CONSTRAINT `fk_FornecedorInsumo_Fornecedores1` FOREIGN KEY (`Fornecedores_codigo`) REFERENCES `fornecedores` (`codigo`),
  CONSTRAINT `fk_FornecedorInsumo_Insumos1` FOREIGN KEY (`Insumos_codigo`) REFERENCES `insumos` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fornecedorinsumo`
--

/*!40000 ALTER TABLE `fornecedorinsumo` DISABLE KEYS */;
/*!40000 ALTER TABLE `fornecedorinsumo` ENABLE KEYS */;

--
-- Table structure for table `funcionarios`
--

DROP TABLE IF EXISTS `funcionarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `funcionarios` (
  `matricula` varchar(10) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL,
  `cpf` varchar(11) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL COMMENT 'Apenas números',
  `nome` varchar(50) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL,
  `sobrenome` varchar(50) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL,
  `sexo` varchar(10) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL COMMENT 'Masculino/Feminino',
  `dtNascimento` date NOT NULL,
  `email` varchar(200) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL,
  `dtCadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Auto gerada, registra a data-hora da criação do registro',
  `status` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Status do funcionario. 0-Bloqueado; 1-Ativo; 2-Desligado; 3-Afastado',
  `telefone` varchar(20) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci DEFAULT NULL COMMENT 'Apenas números',
  `endereco` varchar(200) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci DEFAULT NULL COMMENT 'Rua, av., bairro, apto, bloco, número',
  `cidade` varchar(200) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci DEFAULT NULL COMMENT 'Cidade onde reside',
  `uf` varchar(2) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci DEFAULT NULL COMMENT 'Estado onde reside',
  `cep` varchar(8) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci DEFAULT NULL COMMENT 'CEP da residência',
  `cargos_codigo` int(11) NOT NULL,
  PRIMARY KEY (`matricula`),
  UNIQUE KEY `cpf_UNIQUE` (`cpf`),
  KEY `fk_funcionarios_cargos_idx` (`cargos_codigo`),
  CONSTRAINT `fk_funcionarios_cargos` FOREIGN KEY (`cargos_codigo`) REFERENCES `cargos` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `funcionarios`
--

/*!40000 ALTER TABLE `funcionarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `funcionarios` ENABLE KEYS */;

--
-- Table structure for table `insumocategorias`
--

DROP TABLE IF EXISTS `insumocategorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `insumocategorias` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '1' COMMENT '0-Inativo; 1-Ativo',
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `insumocategorias`
--

/*!40000 ALTER TABLE `insumocategorias` DISABLE KEYS */;
/*!40000 ALTER TABLE `insumocategorias` ENABLE KEYS */;

--
-- Table structure for table `insumos`
--

DROP TABLE IF EXISTS `insumos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `insumos` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL,
  `descricao` varchar(45) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '1' COMMENT '0-Inativo; 1-Ativo',
  `qtdeMinima` double NOT NULL DEFAULT '0',
  `qtdeMaxima` double NOT NULL DEFAULT '0',
  `qtdeAtual` double NOT NULL DEFAULT '0',
  `InsumoCategorias_codigo` int(11) NOT NULL,
  PRIMARY KEY (`codigo`),
  KEY `fk_Insumos_InsumoCategorias1_idx` (`InsumoCategorias_codigo`),
  CONSTRAINT `fk_Insumos_InsumoCategorias1` FOREIGN KEY (`InsumoCategorias_codigo`) REFERENCES `insumocategorias` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `insumos`
--

/*!40000 ALTER TABLE `insumos` DISABLE KEYS */;
/*!40000 ALTER TABLE `insumos` ENABLE KEYS */;

--
-- Table structure for table `necessidadesespeciais`
--

DROP TABLE IF EXISTS `necessidadesespeciais`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `necessidadesespeciais` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL,
  `descricao` varchar(255) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL,
  `requisitos` text CHARACTER SET utf8 COLLATE utf8_0900_ai_ci COMMENT 'Requisitos especiais para atender a necessidade do aluno. Intérprete, tradutor, teclado especial, sala especial, etc.',
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `necessidadesespeciais`
--

/*!40000 ALTER TABLE `necessidadesespeciais` DISABLE KEYS */;
/*!40000 ALTER TABLE `necessidadesespeciais` ENABLE KEYS */;

--
-- Table structure for table `professordisciplina`
--

DROP TABLE IF EXISTS `professordisciplina`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `professordisciplina` (
  `Professores_matricula` varchar(10) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL,
  `Disciplinas_codigo` int(11) NOT NULL,
  PRIMARY KEY (`Professores_matricula`,`Disciplinas_codigo`),
  KEY `fk_ProfessorDisciplina_Disciplinas1_idx` (`Disciplinas_codigo`),
  CONSTRAINT `fk_ProfessorDisciplina_Disciplinas1` FOREIGN KEY (`Disciplinas_codigo`) REFERENCES `disciplinas` (`codigo`),
  CONSTRAINT `fk_ProfessorDisciplina_Professores1` FOREIGN KEY (`Professores_matricula`) REFERENCES `professores` (`matricula`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professordisciplina`
--

/*!40000 ALTER TABLE `professordisciplina` DISABLE KEYS */;
/*!40000 ALTER TABLE `professordisciplina` ENABLE KEYS */;

--
-- Table structure for table `professores`
--

DROP TABLE IF EXISTS `professores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `professores` (
  `matricula` varchar(10) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL,
  `cpf` varchar(11) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL COMMENT 'Apenas números',
  `nome` varchar(50) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL,
  `sobrenome` varchar(50) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL,
  `sexo` varchar(10) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL COMMENT 'Masculino/Feminino',
  `dtNascimento` date NOT NULL,
  `email` varchar(200) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL,
  `dtCadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Auto gerada, registra a data-hora da criação do registro',
  `status` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Status do professor. 0-Bloqueado; 1-Ativo ;2-Desligado; 3-Afastado',
  `telefone` varchar(20) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci DEFAULT NULL COMMENT 'Apenas números',
  `endereco` varchar(200) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci DEFAULT NULL COMMENT 'Rua, av., bairro, apto, bloco, número',
  `cidade` varchar(200) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci DEFAULT NULL COMMENT 'Cidade onde reside',
  `uf` varchar(2) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci DEFAULT NULL COMMENT 'Estado onde reside',
  `cep` varchar(8) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci DEFAULT NULL COMMENT 'CEP da residência',
  `valorHoraAula` double DEFAULT '0',
  `Titulacao_codigo` int(11) NOT NULL,
  PRIMARY KEY (`matricula`),
  UNIQUE KEY `cpf_UNIQUE` (`cpf`),
  KEY `fk_professores_Titulacao1_idx` (`Titulacao_codigo`),
  CONSTRAINT `fk_professores_Titulacao1` FOREIGN KEY (`Titulacao_codigo`) REFERENCES `titulacoes` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professores`
--

/*!40000 ALTER TABLE `professores` DISABLE KEYS */;
/*!40000 ALTER TABLE `professores` ENABLE KEYS */;

--
-- Table structure for table `responsaveis`
--

DROP TABLE IF EXISTS `responsaveis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `responsaveis` (
  `matricula` varchar(10) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL,
  `cpf` varchar(11) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL COMMENT 'Apenas números',
  `nome` varchar(50) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL,
  `sobrenome` varchar(50) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL,
  `sexo` varchar(10) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL COMMENT 'Masculino/Feminino',
  `dtNascimento` date NOT NULL,
  `email` varchar(200) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL,
  `dtCadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Auto gerada, registra a data-hora da criação do registro',
  `status` smallint(6) NOT NULL DEFAULT '1' COMMENT 'Status do responsavel. 0-Bloqueado ;1-Ativo',
  `telefone` varchar(20) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci DEFAULT NULL COMMENT 'Apenas números',
  `endereco` varchar(200) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci DEFAULT NULL COMMENT 'Rua, av., bairro, apto, bloco, número',
  `cidade` varchar(200) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci DEFAULT NULL COMMENT 'Cidade onde reside',
  `uf` varchar(2) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci DEFAULT NULL COMMENT 'Estado onde reside',
  `cep` varchar(8) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci DEFAULT NULL COMMENT 'CEP da residência',
  PRIMARY KEY (`matricula`),
  UNIQUE KEY `cpf_UNIQUE` (`cpf`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `responsaveis`
--

/*!40000 ALTER TABLE `responsaveis` DISABLE KEYS */;
/*!40000 ALTER TABLE `responsaveis` ENABLE KEYS */;

--
-- Table structure for table `salas`
--

DROP TABLE IF EXISTS `salas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `salas` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '1' COMMENT '0-Inativo; 1-Ativo; 2-Em manutenção; 3-Indisponível',
  `SalaTipos_codigo` int(11) NOT NULL,
  PRIMARY KEY (`codigo`),
  KEY `fk_Salas_SalaTipos1_idx` (`SalaTipos_codigo`),
  CONSTRAINT `fk_Salas_SalaTipos1` FOREIGN KEY (`SalaTipos_codigo`) REFERENCES `salatipos` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salas`
--

/*!40000 ALTER TABLE `salas` DISABLE KEYS */;
/*!40000 ALTER TABLE `salas` ENABLE KEYS */;

--
-- Table structure for table `salatipos`
--

DROP TABLE IF EXISTS `salatipos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `salatipos` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '1' COMMENT '0-Inativo; 1-Ativo',
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salatipos`
--

/*!40000 ALTER TABLE `salatipos` DISABLE KEYS */;
/*!40000 ALTER TABLE `salatipos` ENABLE KEYS */;

--
-- Table structure for table `titulacoes`
--

DROP TABLE IF EXISTS `titulacoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `titulacoes` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL COMMENT 'Graduação, Mestre, Doutor, Especialista, etc.',
  `pisoHoraAula` double NOT NULL DEFAULT '0' COMMENT 'Valordo piso salarial para a hora-aula do titulo',
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `titulacoes`
--

/*!40000 ALTER TABLE `titulacoes` DISABLE KEYS */;
/*!40000 ALTER TABLE `titulacoes` ENABLE KEYS */;

--
-- Table structure for table `turmas`
--

DROP TABLE IF EXISTS `turmas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8 ;
CREATE TABLE `turmas` (
  `codigo` varchar(10) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL,
  `nome` varchar(45) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '1' COMMENT '0-Encerrada; 1-Ativo; 2-Confirmada; 3-Em formação; 4-Cancelada; 5-Suspensa',
  `dtInicio` date NOT NULL,
  `dtFim` date NOT NULL,
  `turno` varchar(20) CHARACTER SET utf8 COLLATE utf8_0900_ai_ci NOT NULL,
  `Cursos_codigo` int(11) NOT NULL,
  PRIMARY KEY (`codigo`),
  KEY `fk_Turmas_Cursos1_idx` (`Cursos_codigo`),
  CONSTRAINT `fk_Turmas_Cursos1` FOREIGN KEY (`Cursos_codigo`) REFERENCES `cursos` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `turmas`
--

/*!40000 ALTER TABLE `turmas` DISABLE KEYS */;
/*!40000 ALTER TABLE `turmas` ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-02 19:49:44
