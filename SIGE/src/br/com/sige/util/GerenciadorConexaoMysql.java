package br.com.sige.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class GerenciadorConexaoMysql implements GerenciadorConexao {
	private static GerenciadorConexaoMysql instancia;
	private String url;
	private String user;
	private String pass;
	
	private GerenciadorConexaoMysql()throws ConexaoException {
		try {
			ResourceBundle rb = ResourceBundle.getBundle("br.com.sige.util.banco");
			user = rb.getString("user");
			pass = rb.getString("pass");
			url = rb.getString("url");
			return;
		}catch(MissingResourceException erro) {
			throw new ConexaoException(erro.getCause());
		}
	}
	
	/**
	 * Singleton
	 * @return
	 */
	public static GerenciadorConexao getInstancia()throws ConexaoException {
		if(instancia == null) {
			instancia = new GerenciadorConexaoMysql();
		}
		return instancia;
	}

	@Override
	public Connection getConnection() throws ConexaoException {
		try {
			Connection c = DriverManager.getConnection(url, user, pass);
			return c;
		}catch(SQLException erro) {
			throw new ConexaoException(erro); 
		}
	}

	@Override
	public void closeConnection(Connection c) throws ConexaoException {
		try {
			c.close();
		}catch(SQLException erro) {
			throw new ConexaoException(erro);
		}
	}

}
