package br.com.sige.util;

@SuppressWarnings("serial")
public class ConexaoException extends Exception {

	public ConexaoException() {
		// TODO Auto-generated constructor stub
	}

	public ConexaoException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ConexaoException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public ConexaoException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ConexaoException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
