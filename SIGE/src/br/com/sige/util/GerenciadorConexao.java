package br.com.sige.util;

import java.sql.Connection;

public interface GerenciadorConexao {
	
	/**
	 * Estabele uma conexao com BD e retorna uma Conexao ativa
	 * @return Connection Conexao com o BD
	 * @throws ConexaoException
	 */
	public Connection getConnection() throws ConexaoException ;
	
	
	/**
	 * Encerra a conexao com BD
	 * @param c Uma conexao ativa a ser encerrada
	 * @throws ConexaoException
	 */
	public void closeConnection(Connection c) throws ConexaoException;
}
