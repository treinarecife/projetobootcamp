package br.com.sige.util;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class GerenciadorConexaoDS implements GerenciadorConexao {
	private static GerenciadorConexaoDS instancia;
	private DataSource ds;
	private String ds_name;
	
	private GerenciadorConexaoDS()throws ConexaoException {
		try {
			ResourceBundle rb = ResourceBundle.getBundle("br.com.sige.util.banco");
			ds_name = rb.getString("datasource");
		}catch(MissingResourceException erro) {
			throw new ConexaoException(erro.getCause());
		}
		loadDs();
	}
	
	/**
	 * Tenta encontrar o recurso JNDI do DataSource no Servidor de Aplicação
	 * @throws ConexaoException Se o recurso não for encontrado
	 */
	private void loadDs() throws ConexaoException {
		try {
			Context ctx = new InitialContext();
			this.ds = (DataSource)ctx.lookup(ds_name);
		}catch(NamingException erro) {
			throw new ConexaoException(erro.getCause());
		}
	}
	
	/**
	 * Singleton
	 * @return
	 */
	public static GerenciadorConexao getInstancia()throws ConexaoException {
		if(instancia == null) {
			instancia = new GerenciadorConexaoDS();
		}
		return instancia;
	}

	@Override
	public Connection getConnection() throws ConexaoException {
		try {
			Connection c = this.ds.getConnection();
			return c;
		}catch(SQLException erro) {
			throw new ConexaoException(erro); 
		}
	}

	@Override
	public void closeConnection(Connection c) throws ConexaoException {
		try {
			c.close();
		}catch(SQLException erro) {
			throw new ConexaoException(erro);
		}
	}

}
