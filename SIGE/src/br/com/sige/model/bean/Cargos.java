package br.com.sige.model.bean;

import javax.validation.constraints.NotNull;


public class Cargos {
	
	
	private int codigo;
	
	@NotNull(message="Nome inv�lido")
	private String nome;
	
	@NotNull(message="Descri��o inv�lido")	
	private String descricao;
	
	@NotNull(message="competencias inv�lido")
	
	private String competencias;
	
	@NotNull(message="Piso Salarial n�o informado")
	private double pisoSalarial;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getCompetencias() {
		return competencias;
	}

	public void setCompetencias(String competencias) {
		this.competencias = competencias;
	}

	public double getPisoSalarial() {
		return pisoSalarial;
	}

	public void setPisoSalarial(double pisoSalarial) {
		this.pisoSalarial = pisoSalarial;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	
}
