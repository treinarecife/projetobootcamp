package br.com.sige.model.bean;

import java.sql.Date;

public class Equipamento {
	
	private int codigo;
	private String numeroOrdem;
	private String nome;
	private String descricao;
	private String marca;
	private Date dtAquisicao;
	private double valorCompra;
	private int status;
	private int sala_codigo;
	
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getNumeroOrdem() {
		return numeroOrdem;
	}
	public void setNumeroOrdem(String numeroOrdem) {
		this.numeroOrdem = numeroOrdem;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public Date getDtAquisicao() {
		return dtAquisicao;
	}
	public void setDtAquisicao(Date dtAquisicao) {
		this.dtAquisicao = dtAquisicao;
	}
	public double getValorCompra() {
		return valorCompra;
	}
	public void setValorCompra(double valorCompra) {
		this.valorCompra = valorCompra;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getSala_codigo() {
		return sala_codigo;
	}
	public void setSala_codigo(int sala_codigo) {
		this.sala_codigo = sala_codigo;
	}

}
