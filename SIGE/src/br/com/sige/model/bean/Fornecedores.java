package br.com.sige.model.bean;

import javax.validation.constraints.NotNull;

public class Fornecedores  {
	
	private int codigo;
	
	@NotNull(message="Nome inv�lido")
	private String nome;
		
	private String status;
	
	
	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}



}
