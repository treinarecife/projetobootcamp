package br.com.sige.model.bean;

import javax.validation.constraints.NotNull;

public class Titulacao {
	
	private int codigo;
	
	@NotNull
	private String nome;
	
	private double pisoHoraAula;

	public Titulacao() {
		
	}
	
	public Titulacao(int codigo) {
		setCodigo(codigo);
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public double getPisoHoraAula() {
		return pisoHoraAula;
	}

	public void setPisoHoraAula(double pisoHoraAula) {
		this.pisoHoraAula = pisoHoraAula;
	}


}
