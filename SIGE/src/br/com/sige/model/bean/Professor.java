package br.com.sige.model.bean;

import javax.validation.constraints.NotNull;

public class Professor extends Pessoa {
	private double valorHoraAula;
	
	@NotNull
	private Titulacao titulacao;

	public double getValorHoraAula() {
		return valorHoraAula;
	}

	public void setValorHoraAula(double valorHoraAula) {
		this.valorHoraAula = valorHoraAula;
	}

	public Titulacao getTitulacao() {
		return titulacao;
	}

	public void setTitulacao(Titulacao titulacao) {
		this.titulacao = titulacao;
	}

}
