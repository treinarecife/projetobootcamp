package br.com.sige.model.bean;

import java.util.Date;

public class Funcionario extends Pessoa {
	private int cargaHoraria;
	private double salario;
	private boolean recebeValeTransporte;
	private boolean recebeTicket;
	private Date dtAdmissao;
	public int getCargaHoraria() {
		return cargaHoraria;
	}
	public void setCargaHoraria(int cargaHoraria) {
		this.cargaHoraria = cargaHoraria;
	}
	public double getSalario() {
		return salario;
	}
	public void setSalario(double salario) {
		this.salario = salario;
	}
	public boolean isRecebeValeTransporte() {
		return recebeValeTransporte;
	}
	public void setRecebeValeTransporte(boolean recebeValeTransporte) {
		this.recebeValeTransporte = recebeValeTransporte;
	}
	public boolean isRecebeTicket() {
		return recebeTicket;
	}
	public void setRecebeTicket(boolean recebeTicket) {
		this.recebeTicket = recebeTicket;
	}
	public Date getDtAdmissao() {
		return dtAdmissao;
	}
	public void setDtAdmissao(Date dtAdmissao) {
		this.dtAdmissao = dtAdmissao;
	}
	
}
