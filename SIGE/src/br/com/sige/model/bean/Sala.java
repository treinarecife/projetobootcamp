package br.com.sige.model.bean;

public class Sala {
	
	private int codigo;
	private String nome;
	private String status;
	private int salaTipo_codigo;

	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getSalaTipo_codigo() {
		return salaTipo_codigo;
	}
	public void setSalaTipo_codigo(int salaTipo_codigo) {
		this.salaTipo_codigo = salaTipo_codigo;
	}
	
}
