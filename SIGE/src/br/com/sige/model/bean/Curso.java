package br.com.sige.model.bean;

public class Curso {
	
	private int codigo;
	private String descricao;
	private String nome;
	private int qtdeMinimaAlunos;
	private int qtdeMaximaAlunos;
	private int status;
	private double valorCusto;
	private double valorVenda;
	
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getQtdeMinimaAlunos() {
		return qtdeMinimaAlunos;
	}
	public void setQtdeMinimaAlunos(int qtdeMinimaAlunos) {
		this.qtdeMinimaAlunos = qtdeMinimaAlunos;
	}
	public int getQtdeMaximaAlunos() {
		return qtdeMaximaAlunos;
	}
	public void setQtdeMaximaAlunos(int qtdeMaximaAlunos) {
		this.qtdeMaximaAlunos = qtdeMaximaAlunos;
	}
	public double getValorCusto() {
		return valorCusto;
	}
	public void setValorCusto(double valorCusto) {
		this.valorCusto = valorCusto;
	}
	public double getValorVenda() {
		return valorVenda;
	}
	public void setValorVenda(double valorVenda) {
		this.valorVenda = valorVenda;
	}
	
}
