package br.com.sige.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.com.sige.model.bean.Cargos;
import br.com.sige.util.ConexaoException;
import br.com.sige.util.GerenciadorConexao;
import br.com.sige.util.GerenciadorConexaoMysql;

public class DAOCargosImpl implements DAOCargos {
	//GerenciadorConexao gc;
	private GerenciadorConexao gc;

	public DAOCargosImpl() throws ConexaoException {
		gc = new GerenciadorConexaoMysql();
		//gc = GerenciadorConexaoMysql.getInstancia();
	}

	@Override
	public void inserir(Cargos objeto) throws DAOException, ConexaoException {
		Connection c = this.gc.conectar();
		
		String sql = "INSERT INTO cargos (codigo,nome, descricao, competencias,pisoSalarial) VALUES (?,?,?,?,?)";
		try {
			PreparedStatement stmt = c.prepareStatement(sql);
			stmt.setInt(1, objeto.getCodigo());
			stmt.setString(2, objeto.getNome());
			stmt.setString(3, objeto.getDescricao());
			stmt.setString(4, objeto.getCompetencias());
			stmt.setDouble(5, objeto.getPisoSalarial());
			stmt.executeUpdate();
		}catch(SQLException erro) {
			throw new DAOException(erro);
		}finally {
			gc.desconectar(c);
		}
	}

	@Override
	public void alterar(Cargos objeto) throws DAOException, ConexaoException {		

	}

	@Override
	public Cargos get(int codigo) throws DAOException, ConexaoException {
		Connection con = gc.conectar();
		Cargos objeto = null;
		
		String sql = "Select codigo,nome, descricao, competencias,pisoSalarial from cargos where codigo=?";
		try {
			PreparedStatement pstmt = con.prepareStatement(sql);	
			pstmt.setInt(1, codigo);
			ResultSet rset = pstmt.executeQuery();
			if(rset.next()) {
				objeto = new Cargos();
				objeto.setCodigo(rset.getInt("codigo"));
				objeto.setNome(rset.getString("nome"));
				objeto.setDescricao(rset.getString("descricao"));	
				objeto.setCompetencias(rset.getString("competencias"));	
				objeto.setPisoSalarial(rset.getDouble("pisoSalarial"));	
			}
			return objeto;
		} catch (SQLException erro) {
			throw new DAOException(erro);
		} finally {
			gc.desconectar(con);
		}		
	}

	
	@Override
	public void excluir(int codigo) throws DAOException, ConexaoException {
		Connection con = gc.conectar();
		
		String sql = "Delete from cargos where codigo = ?";
		try {
			PreparedStatement pstmt = con.prepareStatement(sql);	
			pstmt.setInt(1, codigo);
			//pstmt.executeQuery();	
			pstmt.executeUpdate();
			
		} catch (SQLException erro) {
			throw new DAOException(erro);
		} finally {
			gc.desconectar(con);
		}			
		
	}

	@Override
	public List<Cargos> listar() throws DAOException, ConexaoException {
		Connection c = this.gc.conectar();
		ArrayList<Cargos> lista = new ArrayList<Cargos>();
		
		String sql = "SELECT codigo,nome, descricao, competencias,pisoSalarial FROM cargos";
		try {
			Statement stmt = c.createStatement();
			ResultSet rset = stmt.executeQuery(sql);
			Cargos objeto;
			while(rset.next()) {
				objeto = new Cargos();
				
				objeto.setCodigo(rset.getInt("codigo"));
				objeto.setNome(rset.getString("nome"));
				objeto.setDescricao(rset.getString("descricao"));	
				objeto.setCompetencias(rset.getString("competencias"));	
				objeto.setPisoSalarial(rset.getDouble("pisoSalarial"));				
				
				lista.add(objeto);
			}
			return lista;
		}catch(SQLException erro) {
			throw new DAOException(erro);
		}finally {
			gc.desconectar(c);
		}
	}

}
