package br.com.sige.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.com.sige.model.bean.CategoriaInsumo;
import br.com.sige.util.ConexaoException;
import br.com.sige.util.GerenciadorConexao;
import br.com.sige.util.GerenciadorConexaoMysql;


public class DAOCategoriaInsumoImpl implements DAOCategoriaInsumo {
	//GerenciadorConexao gc;
	private GerenciadorConexao gc;

	public DAOCategoriaInsumoImpl() throws ConexaoException {
		gc = new GerenciadorConexaoMysql();
		//gc = GerenciadorConexaoMysql.getInstancia();
	}

	@Override
	public void inserir(CategoriaInsumo objeto) throws DAOException, ConexaoException {
		Connection c = this.gc.conectar();
		
		String sql = "INSERT INTO insumocategorias (codigo,nome, status) VALUES (?,?,?)";
		try {
			PreparedStatement stmt = c.prepareStatement(sql);
			stmt.setInt(1, objeto.getCodigo());
			stmt.setString(2, objeto.getNome());
			stmt.setString(3, objeto.getStatus());			
			stmt.executeUpdate();
		}catch(SQLException erro) {
			throw new DAOException(erro);
		}finally {
			gc.desconectar(c);
		}
	}

	@Override
	public void alterar(CategoriaInsumo objeto) throws DAOException, ConexaoException {		

	}

	@Override
	public CategoriaInsumo get(int codigo) throws DAOException, ConexaoException {
		Connection con = gc.conectar();
		CategoriaInsumo objeto = null;
		
		String sql = "Select codigo,nome, status from insumocategorias where codigo=?";
		try {
			PreparedStatement pstmt = con.prepareStatement(sql);	
			pstmt.setInt(1, codigo);
			ResultSet rset = pstmt.executeQuery();
			if(rset.next()) {
				objeto = new CategoriaInsumo();
				objeto.setCodigo(rset.getInt("codigo"));
				objeto.setNome(rset.getString("nome"));
				objeto.setStatus(rset.getString("status"));				
			}
			return objeto;
		} catch (SQLException erro) {
			throw new DAOException(erro);
		} finally {
			gc.desconectar(con);
		}		
	}

	
	@Override
	public void excluir(int codigo) throws DAOException, ConexaoException {
		Connection con = gc.conectar();
		
		String sql = "Delete from insumocategorias where codigo = ?";
		try {
			PreparedStatement pstmt = con.prepareStatement(sql);	
			pstmt.setInt(1, codigo);
			//pstmt.executeQuery();	
			pstmt.executeUpdate();
			
		} catch (SQLException erro) {
			throw new DAOException(erro);
		} finally {
			gc.desconectar(con);
		}			
		
	}

	@Override
	public List<CategoriaInsumo> listar() throws DAOException, ConexaoException {
		Connection c = this.gc.conectar();
		ArrayList<CategoriaInsumo> lista = new ArrayList<CategoriaInsumo>();
		
		String sql = "SELECT codigo,nome, status FROM insumocategorias";
		try {
			Statement stmt = c.createStatement();
			ResultSet rset = stmt.executeQuery(sql);
			CategoriaInsumo objeto;
			while(rset.next()) {
				objeto = new CategoriaInsumo();
				
				objeto.setCodigo(rset.getInt("codigo"));
				objeto.setNome(rset.getString("nome"));
				objeto.setStatus(rset.getString("status"));						
				
				lista.add(objeto);
			}
			return lista;
		}catch(SQLException erro) {
			throw new DAOException(erro);
		}finally {
			gc.desconectar(c);
		}
	}

}
