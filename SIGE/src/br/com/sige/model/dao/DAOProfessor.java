package br.com.sige.model.dao;

import java.util.ArrayList;

import br.com.sige.model.bean.Professor;
import br.com.sige.util.ConexaoException;

public interface DAOProfessor {
	
	/**
	 * Insere um novo registro no banco de dados.
	 * @param professor Objeto com os dados validados.
	 * @throws DAOException Erro na camada de model.
	 * @throws ConexaoException Erro na conexao com o banco.
	 */
	public void inserir(Professor professor) throws DAOException, ConexaoException;
	
	/**
	 * Altera um registro no banco de dados caso exista.
	 * @param professor Objeto com os dados validados.
	 * @throws DAOException Erro na camada de model.
	 * @throws ConexaoException Erro na conexao com o banco.
	 */
	public void alterar(Professor professor) throws DAOException, ConexaoException;
	
	/**
	 * Retorna um registro no banco de dados caso exista.
	 * @param matricula atributo identificador do registro de professor.
	 * @return registro de professor, caso exista.
	 * @throws DAOException Erro na camada de model.
	 * @throws ConexaoException Erro na conexao com o banco.
	 */
	public Professor get(String matricula) throws DAOException, ConexaoException;
	
	/**
	 * Retorna uma lista de todos os professores cadastrados no banco de dados.
	 * @return Lista de todos os registros.
	 * @throws DAOException Erro na camada de model.
	 * @throws ConexaoException Erro na conexao com o banco.
	 */
	public ArrayList<Professor> listar() throws DAOException, ConexaoException;
	
	/**
	 * Deleta um registro no banco de dados, caso exista.
	 * @param matricula atributo identificador do registro de professor.
	 * @throws DAOException Erro na camada de model.
	 * @throws ConexaoException Erro na conexao com o banco.
	 */
	public void excluir(String matricula) throws DAOException, ConexaoException;

}
