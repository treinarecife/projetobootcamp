package br.com.sige.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import br.com.sige.model.bean.Sala;
import br.com.sige.util.ConexaoException;
import br.com.sige.util.GerenciadorConexao;
import br.com.sige.util.GerenciadorConexaoMysql;

public class DAOSalaImpl implements DAOSala{
	
	private GerenciadorConexao gc;
	
	public DAOSalaImpl() throws ConexaoException {
		gc = GerenciadorConexaoMysql.getInstancia();
	}
	
	public GerenciadorConexao getGc() {
		return gc;
	}

	public void setGc(GerenciadorConexao gc) {
		this.gc = gc;
	}
	
	@Override
	public List<Sala> listar() throws ConexaoException, DAOException {
		
		Connection con = gc.getConnection();
		ArrayList<Sala> lista = new ArrayList<Sala>();
		
		String sql = "SELECT codigo, nome, status, SalaTipos_codigo FROM sice.salas";
		
		try {
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			
			Sala objeto;
			while(rs.next()) {
				objeto = new Sala();
				objeto.setCodigo(rs.getInt("codigo"));
				objeto.setNome(rs.getString("nome"));
				objeto.setStatus(rs.getString("status"));
				objeto.setSalaTipo_codigo(rs.getInt("SalaTipos_codigo"));
				lista.add(objeto);
			}
			return lista;
		}
		catch(SQLException erro) {
			throw new DAOException(erro);
		}
		finally {
			gc.closeConnection(con);
		}
	
	}
	
	@Override
	public void excluir(int codigo) throws ConexaoException, DAOException {
		
		Connection con = gc.getConnection();
		String sql = "DELETE FROM sice.salas WHERE codigo = ?";
		
		try {
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, codigo);
			pstmt.executeUpdate();
		}
		catch(SQLException erro) {
			throw new DAOException(erro);
		}
		finally {
			gc.closeConnection(con);
		}
	
	}
	
	@Override
	public void editar(Sala sala) throws ConexaoException, DAOException {

		Connection con = gc.getConnection();

		String sql = "UPDATE sice.salas SET nome=?, status=?, SalaTipos_codigo=? WHERE codigo=?";
		
		try {
			PreparedStatement prstm = con.prepareStatement(sql);
			prstm.setInt(4, sala.getCodigo());
			prstm.setString(1, sala.getNome());
			prstm.setString(2, sala.getStatus());
			prstm.setInt(3, sala.getSalaTipo_codigo());
			prstm.executeUpdate();
		}
		catch(SQLException erro) {
			throw new DAOException(erro);
		}
		finally {
			gc.closeConnection(con);
		}

	}
	
	@Override
	public void inserir(Sala sala) throws ConexaoException, DAOException {

		Connection con = gc.getConnection();

		String sql = "INSERT INTO sice.salas (codigo, nome, status, SalaTipos_codigo) VALUES (?,?,?,?)";
		
		try {
			PreparedStatement prstm = con.prepareStatement(sql);
			prstm.setInt(1, sala.getCodigo());
			prstm.setString(2, sala.getNome());
			prstm.setString(3, sala.getStatus());
			prstm.setInt(4, sala.getSalaTipo_codigo());
			prstm.executeUpdate();
		}
		catch(SQLException erro) {
			throw new DAOException(erro);
		}
		finally {
			gc.closeConnection(con);
		}
	
	}

	
}
