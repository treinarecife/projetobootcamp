package br.com.sige.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.com.sige.util.GerenciadorConexao;
import br.com.sige.util.GerenciadorConexaoMysql;
import br.com.sige.model.bean.Titulacao;
import br.com.sige.util.ConexaoException;

public class DAOTitulacaoImpl implements DAOTitulacao {
	private GerenciadorConexao gc;

	public DAOTitulacaoImpl() throws ConexaoException{
		gc = GerenciadorConexaoMysql.getInstancia();
	}

	@Override
	public void inserir(Titulacao obj) throws DAOException, ConexaoException {
		Connection con = gc.getConnection();

		String sql = "INSERT INTO titulacoes (nome,pisoHoraAula) VALUES(?,?)";
		try {
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, obj.getNome());
			stmt.setDouble(2, obj.getPisoHoraAula());
			stmt.executeUpdate();
		} catch (SQLException erro) {
			throw new DAOException(erro);
		} finally {
			gc.closeConnection(con);
		}
	}

	@Override
	public void alterar(Titulacao obj) throws DAOException, ConexaoException {
		Connection con = gc.getConnection();

		String sql = "UPDATE titulacoes SET nome=?, pisoHoraAula=? WHERE codigo=?";
		try {
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, obj.getNome());
			stmt.setDouble(2, obj.getPisoHoraAula());
			stmt.setInt(3, obj.getCodigo());
			stmt.executeUpdate();
		} catch (SQLException erro) {
			throw new DAOException(erro);
		} finally {
			gc.closeConnection(con);
		}
	}
	
	@Override
	public void excluir(Titulacao obj) throws DAOException, ConexaoException {
		Connection con = gc.getConnection();

		String sql = "DELETE FROM titulacoes WHERE codigo=?";
		try {
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, obj.getCodigo());
			stmt.executeUpdate();
		} catch (SQLException erro) {
			throw new DAOException(erro);
		} finally {
			gc.closeConnection(con);
		}		
	}

	@Override
	public List<Titulacao> listar() throws ConexaoException, DAOException {
		Connection con = gc.getConnection();
		List<Titulacao> lista = new ArrayList<Titulacao>();
		
		String sql = "SELECT codigo,nome,pisoHoraAula FROM titulacoes";
		try {
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			
			Titulacao obj;
			while(rs.next()) {
				obj = new Titulacao();
				obj.setCodigo(rs.getInt("codigo"));
				obj.setNome(rs.getString("nome"));
				obj.setPisoHoraAula(rs.getDouble("pisoHoraAula"));
				lista.add(obj);
			}
			return lista;
		} catch (SQLException erro) {
			throw new DAOException(erro);
		} finally {
			gc.closeConnection(con);
		}
	}

	@Override
	public Titulacao get(Titulacao obj) throws DAOException, ConexaoException {
		Connection con = gc.getConnection();
		Titulacao titulacao = null;
		
		String sql = "SELECT * FROM titulacoes WHERE codigo=?";
		try {
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setInt(1, obj.getCodigo());
			ResultSet rs = stmt.executeQuery();
			
			if(rs.next()) {
				titulacao = new Titulacao();
				titulacao.setCodigo(rs.getInt("codigo"));
				titulacao.setNome(rs.getString("nome"));
				titulacao.setPisoHoraAula(rs.getDouble("pisoHoraAula"));
			}
			return titulacao;
		} catch (SQLException erro) {
			throw new DAOException(erro);
		} finally {
			gc.closeConnection(con);
		}
	}
	
	@Override
	public Titulacao get(int codigo)throws DAOException, ConexaoException{
		Titulacao titulacao = new Titulacao(codigo);
		return get(titulacao);
	}

}
