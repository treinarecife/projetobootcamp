package br.com.sige.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.com.sige.model.bean.Fornecedores;
import br.com.sige.util.ConexaoException;
import br.com.sige.util.GerenciadorConexao1;
import br.com.sige.util.GerenciadorConexaoMysql;

public class DAOFornecedoresImpl implements DAOFornecedores {
	//GerenciadorConexao gc;
	private GerenciadorConexao1 gc;

	public DAOFornecedoresImpl() throws ConexaoException {
		gc = new GerenciadorConexaoMysql();
		//gc = GerenciadorConexaoMysql.getInstancia();
	}

	@Override
	public void inserir(Fornecedores objeto) throws DAOException, ConexaoException {
		Connection c = this.gc.conectar();
		
		String sql = "INSERT INTO fornecedores (codigo,nome, status) VALUES (?,?,?)";
		try {
			PreparedStatement stmt = c.prepareStatement(sql);
			stmt.setInt(1, objeto.getCodigo());
			stmt.setString(2, objeto.getNome());
			stmt.setString(3, objeto.getStatus());			
			stmt.executeUpdate();
		}catch(SQLException erro) {
			throw new DAOException(erro);
		}finally {
			gc.desconectar(c);
		}
	}

	@Override
	public void alterar(Fornecedores objeto) throws DAOException, ConexaoException {		

	}

	@Override
	public Fornecedores get(int codigo) throws DAOException, ConexaoException {
		Connection con = gc.conectar();
		Fornecedores objeto = null;
		
		String sql = "Select codigo,nome, status from fornecedores where codigo=?";
		try {
			PreparedStatement pstmt = con.prepareStatement(sql);	
			pstmt.setInt(1, codigo);
			ResultSet rset = pstmt.executeQuery();
			if(rset.next()) {
				objeto = new Fornecedores();
				objeto.setCodigo(rset.getInt("codigo"));
				objeto.setNome(rset.getString("nome"));
				objeto.setStatus(rset.getString("status"));				
			}
			return objeto;
		} catch (SQLException erro) {
			throw new DAOException(erro);
		} finally {
			gc.desconectar(con);
		}		
	}

	
	@Override
	public void excluir(int codigo) throws DAOException, ConexaoException {
		Connection con = gc.conectar();
		
		String sql = "Delete from fornecedores where codigo = ?";
		try {
			PreparedStatement pstmt = con.prepareStatement(sql);	
			pstmt.setInt(1, codigo);
			//pstmt.executeQuery();	
			pstmt.executeUpdate();
			
		} catch (SQLException erro) {
			throw new DAOException(erro);
		} finally {
			gc.desconectar(con);
		}			
		
	}

	@Override
	public List<Fornecedores> listar() throws DAOException, ConexaoException {
		Connection c = this.gc.conectar();
		ArrayList<Fornecedores> lista = new ArrayList<Fornecedores>();
		
		String sql = "SELECT codigo,nome, status FROM fornecedores";
		try {
			Statement stmt = c.createStatement();
			ResultSet rset = stmt.executeQuery(sql);
			Fornecedores objeto;
			while(rset.next()) {
				objeto = new Fornecedores();
				
				objeto.setCodigo(rset.getInt("codigo"));
				objeto.setNome(rset.getString("nome"));
				objeto.setStatus(rset.getString("status"));						
				
				lista.add(objeto);
			}
			return lista;
		}catch(SQLException erro) {
			throw new DAOException(erro);
		}finally {
			gc.desconectar(c);
		}
	}

}
