package br.com.sige.model.dao;

import java.util.List;
import br.com.sige.model.bean.Disciplina;
import br.com.sige.util.ConexaoException;

public interface DAODisciplina {
	
	/**
	 * Faz uma consulta sem filtros, retornando todos os dados do BD
	 * @return Lista com todos os registros
	 * @throws ConexaoException 
	 * @throws DAOException
	 */
	public List<Disciplina> listar() throws ConexaoException, DAOException;
	
	/**
	 * Exclui registro de disciplina do BD
	 * @param Objeto disciplina a ser exclu�do
	 * @throws ConexaoException
	 * @throws DAOException 
	 */
	public void excluir(int codigo) throws ConexaoException, DAOException;
	
	/**
	 * Persiste uma atualiza��o do registro no BD
	 * @param Objeto disciplina com os dados validados
	 * @throws ConexaoException
	 * @throws DAOException 
	 */
	public void editar(Disciplina disciplina) throws ConexaoException, DAOException;
	
	/**
	 * Persiste um novo registro no BD
	 * @param Objeto disciplina com os dados validados
	 * @throws ConexaoException
	 * @throws DAOException 
	 */
	public void inserir(Disciplina disciplina) throws ConexaoException, DAOException;

}
