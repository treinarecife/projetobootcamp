package br.com.sige.model.dao;

import java.util.List;

import br.com.sige.model.bean.Funcionario;
import br.com.sige.util.ConexaoException;

public interface DAOFuncionario {
	/**
	 * Fun��o respons�vel por buscar a listagem dos funcion�rios da institui��o
	 * @return uma lista com os funcion�rios encontrados
	 * @throws ConexaoException Erro caso o gereneciador n�o consiga estabelecer conex�o	
	 * @throws DAOException Erro caso o DAO n�o consiga obter os par�metros requeridos
	 */
	public List<Funcionario> listar() throws ConexaoException, DAOException;

}
