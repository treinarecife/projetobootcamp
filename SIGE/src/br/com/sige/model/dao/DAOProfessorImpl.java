package br.com.sige.model.dao;

import java.sql.Connection;
import java.util.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import br.com.sige.model.bean.Professor;
import br.com.sige.util.ConexaoException;
import br.com.sige.util.GerenciadorConexao;
import br.com.sige.util.GerenciadorConexaoMysql;

public class DAOProfessorImpl implements DAOProfessor {
	GerenciadorConexao gc;

	public DAOProfessorImpl() throws ConexaoException {
		gc = GerenciadorConexaoMysql.getInstancia();
	}

	@Override
	public void inserir(Professor professor) throws DAOException, ConexaoException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void alterar(Professor professor) throws DAOException, ConexaoException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Professor get(String matricula) throws DAOException, ConexaoException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<Professor> listar() throws DAOException, ConexaoException {
		Connection c = this.gc.getConnection();
		ArrayList<Professor> lista = new ArrayList<Professor>();
		
		//List SQL
		String sql = "Select * from professores";
		try {
			Statement stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			Professor prof;
			
			while(rs.next()) {
//				SET DATA TYPES
				SimpleDateFormat formatNascimento = new SimpleDateFormat("yyyy-MM-dd");
				SimpleDateFormat formatCadastro = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date dtNascimento = null;
				Date dtCadastro = null;
				try {
					dtNascimento = formatNascimento.parse(rs.getString("dtNascimento")); 
					dtCadastro = formatCadastro.parse(rs.getString("dtCadastro"));					
				} catch(Exception erro) {
					System.out.println(erro);
				}
				
//				SET PROFESSOR
				prof = new Professor();
				prof.setMatricula(rs.getString("matricula"));
				prof.setCpf(rs.getString("cpf"));
				prof.setNome(rs.getString("nome"));
				prof.setSobrenome(rs.getString("sobrenome"));
				prof.setSexo(rs.getString("sexo"));
				prof.setDtNascimento(dtNascimento);
				prof.setEmail(rs.getString("email"));
				prof.setDtCadastro(dtCadastro);
				prof.setStatus(Integer.parseInt(rs.getString("status")));
				prof.setTelefone(rs.getString("telefone"));
				prof.setEndereco(rs.getString("endereco"));
				prof.setCidade(rs.getString("cidade"));
				prof.setUf(rs.getString("uf"));
				prof.setCep(rs.getString("cep"));
				prof.setValorHoraAula(Double.parseDouble(rs.getString("valorHoraAula")));
				
//				SET TITULA��O
				DAOTitulacao dao = new DAOTitulacaoImpl();
				prof.setTitulacao(dao.get(rs.getInt("Titulacao_codigo")));
				
				
				lista.add(prof);
			}
			
			return lista;
			
		} catch (SQLException erro) {
			throw new DAOException(erro);
		} finally {
			this.gc.closeConnection(c);
		}
	}

	@Override
	public void excluir(String matricula) throws DAOException, ConexaoException {
		// TODO Auto-generated method stub
		
	}

}
