package br.com.sige.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import br.com.sige.model.bean.CalendarioLetivo;
import br.com.sige.util.ConexaoException;
import br.com.sige.util.GerenciadorConexao;
import br.com.sige.util.GerenciadorConexaoMysql;

public class DAOCalendarioLetivoImpl implements DAOCalendarioLetivo{
	
	private GerenciadorConexao gc;
	
	public DAOCalendarioLetivoImpl() throws ConexaoException {
		gc = GerenciadorConexaoMysql.getInstancia();
	}
	
	public GerenciadorConexao getGc() {
		return gc;
	}

	public void setGc(GerenciadorConexao gc) {
		this.gc = gc;
	}
	
	@Override
	public List<CalendarioLetivo> listar() throws ConexaoException, DAOException {
		
		Connection con = gc.getConnection();
		ArrayList<CalendarioLetivo> lista = new ArrayList<CalendarioLetivo>();
		
		String sql = "SELECT ano, dtInicio, dtFim, descricao, evento FROM sice.calendariosletivos";
		
		try {
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			
			CalendarioLetivo objeto;
			while(rs.next()) {
				objeto = new CalendarioLetivo();
				objeto.setAno(rs.getInt("ano"));
				objeto.setDtInicio(rs.getDate("dtInicio"));
				objeto.setDtFim(rs.getDate("dtFim"));
				objeto.setDescricao(rs.getString("descricao"));
				objeto.setEvento(rs.getString("evento"));
				lista.add(objeto);
			}
			return lista;
		}
		catch(SQLException erro) {
			throw new DAOException(erro);
		}
		finally {
			gc.closeConnection(con);
		}
	
	}
	
	@Override
	public void excluir(int codigo) throws ConexaoException, DAOException {
		
		Connection con = gc.getConnection();
		String sql = "DELETE FROM sice.calendariosletivos WHERE codigo = ?";
		
		try {
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, codigo);
			pstmt.executeUpdate();
		}
		catch(SQLException erro) {
			throw new DAOException(erro);
		}
		finally {
			gc.closeConnection(con);
		}
	
	}
	
	@Override
	public void editar(CalendarioLetivo evento) throws ConexaoException, DAOException {

		Connection con = gc.getConnection();

		String sql = "UPDATE sice.calendariosletivos SET ano=?, dtInicio=?, dtFim=?, descricao=?, evento=? WHERE codigo=?";
		
		try {
			PreparedStatement prstm = con.prepareStatement(sql);
			prstm.setInt(6, evento.getCodigo());
			prstm.setInt(1, evento.getAno());
			prstm.setDate(2, evento.getDtInicio());
			prstm.setDate(3, evento.getDtFim());
			prstm.setString(4, evento.getDescricao());
			prstm.setString(5, evento.getEvento());
			prstm.executeUpdate();
		}
		catch(SQLException erro) {
			throw new DAOException(erro);
		}
		finally {
			gc.closeConnection(con);
		}

	}
	
	@Override
	public void inserir(CalendarioLetivo evento) throws ConexaoException, DAOException {

		Connection con = gc.getConnection();

		String sql = "INSERT INTO sice.calendariosletivos (codigo, ano, dtInicio, dtFim, descricao, evento) VALUES (?,?,?,?,?,?)";
		
		try {
			PreparedStatement prstm = con.prepareStatement(sql);
			prstm.setInt(1, evento.getCodigo());
			prstm.setInt(2, evento.getAno());
			prstm.setDate(3, evento.getDtInicio());
			prstm.setDate(4, evento.getDtFim());
			prstm.setString(5, evento.getDescricao());
			prstm.setString(6, evento.getEvento());
			prstm.executeUpdate();
		}
		catch(SQLException erro) {
			throw new DAOException(erro);
		}
		finally {
			gc.closeConnection(con);
		}
	
	}

	
}
