package br.com.sige.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import br.com.sige.model.bean.Equipamento;
import br.com.sige.util.ConexaoException;
import br.com.sige.util.GerenciadorConexao;
import br.com.sige.util.GerenciadorConexaoMysql;

public class DAOEquipamentoImpl implements DAOEquipamento{
	
	private GerenciadorConexao gc;
	
	public DAOEquipamentoImpl() throws ConexaoException {
		gc = GerenciadorConexaoMysql.getInstancia();
	}
	
	public GerenciadorConexao getGc() {
		return gc;
	}

	public void setGc(GerenciadorConexao gc) {
		this.gc = gc;
	}
	
	@Override
	public List<Equipamento> listar() throws ConexaoException, DAOException {
		
		Connection con = gc.getConnection();
		ArrayList<Equipamento> lista = new ArrayList<Equipamento>();
		
		String sql = "SELECT codigo, nome, status FROM sice.equipamentos";
		
		try {
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			
			Equipamento objeto;
			while(rs.next()) {
				objeto = new Equipamento();
				objeto.setCodigo(rs.getInt("codigo"));
				objeto.setNome(rs.getString("nome"));
				objeto.setStatus(rs.getInt("status"));
				lista.add(objeto);
			}
			return lista;
		}
		catch(SQLException erro) {
			throw new DAOException(erro);
		}
		finally {
			gc.closeConnection(con);
		}
	
	}
	
	@Override
	public void excluir(int codigo) throws ConexaoException, DAOException {
		
		Connection con = gc.getConnection();
		String sql = "DELETE FROM sice.salas WHERE codigo = ?";
		
		try {
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, codigo);
			pstmt.executeUpdate();
		}
		catch(SQLException erro) {
			throw new DAOException(erro);
		}
		finally {
			gc.closeConnection(con);
		}
	
	}
	
	@Override
	public void editar(Equipamento equipamento) throws ConexaoException, DAOException {

		Connection con = gc.getConnection();

		String sql = "UPDATE sice.equipamentos SET nome=?, status=?, descricao=?, marca=?, dtAquisicao=?, numeroOrdem=?, valorCompra=?, Salas_codigo=? WHERE codigo=?";
		
		try {
			PreparedStatement prstm = con.prepareStatement(sql);
			prstm.setInt(9, equipamento.getCodigo());
			prstm.setString(1, equipamento.getNome());
			prstm.setInt(2, equipamento.getStatus());
			prstm.setString(3, equipamento.getDescricao());
			prstm.setString(4, equipamento.getMarca());
			prstm.setDate(5, equipamento.getDtAquisicao());
			prstm.setString(6, equipamento.getNumeroOrdem());
			prstm.setDouble(7, equipamento.getValorCompra());
			prstm.setInt(8, equipamento.getSala_codigo());

			
			prstm.executeUpdate();
		}
		catch(SQLException erro) {
			throw new DAOException(erro);
		}
		finally {
			gc.closeConnection(con);
		}

	}
	
	@Override
	public void inserir(Equipamento equipamento) throws ConexaoException, DAOException {

		Connection con = gc.getConnection();

		String sql = "INSERT INTO sice.equipamentos (codigo, nome, status, descricao, marca, dtAquisicao, numeroOrdem, valorCompra, Salas_codigo) VALUES (?,?,?,?,?,?,?,?,?)";
		
		try {
			PreparedStatement prstm = con.prepareStatement(sql);
			prstm.setInt(1, equipamento.getCodigo());
			prstm.setString(2, equipamento.getNome());
			prstm.setInt(3, equipamento.getStatus());
			prstm.setString(4, equipamento.getDescricao());
			prstm.setString(5, equipamento.getMarca());
			prstm.setDate(6, equipamento.getDtAquisicao());
			prstm.setString(7, equipamento.getNumeroOrdem());
			prstm.setDouble(8, equipamento.getValorCompra());
			prstm.setInt(9, equipamento.getSala_codigo());
			prstm.executeUpdate();
		}
		catch(SQLException erro) {
			throw new DAOException(erro);
		}
		finally {
			gc.closeConnection(con);
		}
	
	}

	
}
