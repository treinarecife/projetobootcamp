package br.com.sige.model.dao;

import java.util.List;
import br.com.sige.model.bean.Sala;
import br.com.sige.util.ConexaoException;

public interface DAOSala {
	
	/**
	 * Faz uma consulta sem filtros, retornando todos os dados do BD
	 * @return Lista com todos os registros
	 * @throws ConexaoException 
	 * @throws DAOException
	 */
	public List<Sala> listar() throws ConexaoException, DAOException;
	
	/**
	 * Exclui registro de sala do BD
	 * @param Objeto sala a ser exclu�do
	 * @throws ConexaoException
	 * @throws DAOException 
	 */
	public void excluir(int codigo) throws ConexaoException, DAOException;
	
	/**
	 * Persiste uma atualiza��o do registro no BD
	 * @param Objeto sala com os dados validados
	 * @throws ConexaoException
	 * @throws DAOException 
	 */
	public void editar(Sala sala) throws ConexaoException, DAOException;
	
	/**
	 * Persiste um novo registro no BD
	 * @param Objeto sala com os dados validados
	 * @throws ConexaoException
	 * @throws DAOException 
	 */
	public void inserir(Sala sala) throws ConexaoException, DAOException;

}
