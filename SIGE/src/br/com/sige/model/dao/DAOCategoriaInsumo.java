package br.com.sige.model.dao;

import java.util.List;

import br.com.sige.model.bean.CategoriaInsumo;
import br.com.sige.util.ConexaoException;

public interface DAOCategoriaInsumo {
	
	/**
	 * Persiste um novo registro no BD
	 * @param objeto Fornecedor com os dados validados
	 * @throws DAOException Caso o programador tenha feito besteira
	 * @throws ConexaoException
	 */
	public void inserir(CategoriaInsumo objeto) throws DAOException, ConexaoException;

	/**
	 * Persiste uma atualiza��o registro no BD
	 * @param objeto Fornecedor com os dados validados
	 * @throws DAOException Caso o programador tenha feito besteira
	 * @throws ConexaoException
	 */
	public void alterar(CategoriaInsumo objeto) throws DAOException, ConexaoException;
	
	/**
	 * Faz a busca no BD pela matr�cula e retorna o Aluno
	 * @param matricula Chave da consulta
	 * @return Obejto Fornecedor com os dados preenchidos ou NULL
	 * @throws DAOException Caso o programador tenha feito besteira
	 * @throws ConexaoException
	 */
	public CategoriaInsumo get(int codigo) throws DAOException, ConexaoException;
	
	/**
	 * Faz um consulta sem filtros, retornando todos os dados do BD 
	 * @return Lista com todos os registros
	 * @throws DAOException Caso o programador tenha feito besteira
	 * @throws ConexaoException
	 */
	public List<CategoriaInsumo> listar() throws DAOException, ConexaoException;
	
	/**
	 * Remove um registro no BD, com base na matr�cula passada
	 * @param matricula Chave da exclus�o
	 * @throws DAOException Caso o programador tenha feito besteira
	 * @throws ConexaoException
	 */
	public void excluir(int codigo) throws DAOException, ConexaoException;
}
