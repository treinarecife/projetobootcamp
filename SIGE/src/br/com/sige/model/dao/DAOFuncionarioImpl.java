package br.com.sige.model.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.com.sige.model.bean.Funcionario;
import br.com.sige.util.ConexaoException;
import br.com.sige.util.GerenciadorConexao;
import br.com.sige.util.GerenciadorConexaoMysql;

public class DAOFuncionarioImpl implements DAOFuncionario {

	private GerenciadorConexao gc;
	public DAOFuncionarioImpl() throws ConexaoException {
		gc = GerenciadorConexaoMysql.getInstancia();
	}
	@Override
	public List<Funcionario> listar() throws ConexaoException, DAOException {
		Connection con = gc.getConnection();
		List<Funcionario> lista = new ArrayList<Funcionario>();
		String sql = "SELECT matricula, cpf, nome, sobrenome, email, status, telefone FROM funcionarios";
		
		try {
			Statement stmt = con.createStatement();
			ResultSet rs  = stmt.executeQuery(sql);
			Funcionario obj;
			
			while(rs.next()) {
				obj = new Funcionario();
				obj.setMatricula(rs.getString("matricula"));
				obj.setCpf(rs.getString("cpf"));
				obj.setNome(rs.getString("nome"));
				obj.setSobrenome(rs.getString("sobrenome"));
				obj.setEmail(rs.getString("email"));
				obj.setStatus(rs.getInt("status"));
				obj.setTelefone(rs.getString("telefone"));
				lista.add(obj);
			}
			return lista;
		} catch (SQLException erro) {
			throw new DAOException(erro);
		}finally {
			gc.closeConnection(con);
		}
	}

}
