package br.com.sige.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import br.com.sige.model.bean.Disciplina;
import br.com.sige.util.ConexaoException;
import br.com.sige.util.GerenciadorConexao;
import br.com.sige.util.GerenciadorConexaoMysql;

public class DAODisciplinaImpl implements DAODisciplina{
	
	private GerenciadorConexao gc;
	
	public DAODisciplinaImpl() throws ConexaoException {
		gc = GerenciadorConexaoMysql.getInstancia();
	}
	
	public GerenciadorConexao getGc() {
		return gc;
	}

	public void setGc(GerenciadorConexao gc) {
		this.gc = gc;
	}
	
	@Override
	public List<Disciplina> listar() throws ConexaoException, DAOException {
		
		Connection con = gc.getConnection();
		ArrayList<Disciplina> lista = new ArrayList<Disciplina>();
		
		String sql = "SELECT codigo, nome, status, ementa FROM sice.disciplinas";
		
		try {
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			
			Disciplina objeto;
			while(rs.next()) {
				objeto = new Disciplina();
				objeto.setCodigo(rs.getInt("codigo"));
				objeto.setNome(rs.getString("nome"));
				objeto.setStatus(rs.getInt("status"));
				objeto.setEmenta(rs.getString("ementa"));
				lista.add(objeto);
			}
			return lista;
		}
		catch(SQLException erro) {
			throw new DAOException(erro);
		}
		finally {
			gc.closeConnection(con);
		}
	
	}
	
	@Override
	public void excluir(int codigo) throws ConexaoException, DAOException {
		
		Connection con = gc.getConnection();
		String sql = "DELETE FROM sice.disciplinas WHERE codigo = ?";
		
		try {
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setInt(1, codigo);
			pstmt.executeUpdate();
		}
		catch(SQLException erro) {
			throw new DAOException(erro);
		}
		finally {
			gc.closeConnection(con);
		}
	
	}
	
	@Override
	public void editar(Disciplina disciplina) throws ConexaoException, DAOException {

		Connection con = gc.getConnection();

		String sql = "UPDATE sice.disciplinas SET nome=?, status=?, ementa=? WHERE codigo=?";
		
		try {
			PreparedStatement prstm = con.prepareStatement(sql);
			prstm.setInt(4, disciplina.getCodigo());
			prstm.setString(1, disciplina.getNome());
			prstm.setInt(2, disciplina.getStatus());
			prstm.setString(3, disciplina.getEmenta());
			prstm.executeUpdate();
		}
		catch(SQLException erro) {
			throw new DAOException(erro);
		}
		finally {
			gc.closeConnection(con);
		}

	}
	
	@Override
	public void inserir(Disciplina disciplina) throws ConexaoException, DAOException {

		Connection con = gc.getConnection();

		String sql = "INSERT INTO sice.disciplinas (codigo, nome, status, descricao, ementa, bibliografia, cargaHoraria) VALUES (?,?,?,?,?,?,?)";
		
		try {
			PreparedStatement prstm = con.prepareStatement(sql);
			prstm.setInt(1, disciplina.getCodigo());
			prstm.setString(2, disciplina.getNome());
			prstm.setInt(3, disciplina.getStatus());
			prstm.setString(4, disciplina.getDescricao());
			prstm.setString(5, disciplina.getEmenta());
			prstm.setString(6, disciplina.getBibliografia());
			prstm.setDouble(7, disciplina.getCargaHoraria());
			prstm.executeUpdate();
		}
		catch(SQLException erro) {
			throw new DAOException(erro);
		}
		finally {
			gc.closeConnection(con);
		}
	
	}

	
}
