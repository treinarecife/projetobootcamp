package br.com.sige.model.dao;

import br.com.sige.model.bean.Titulacao;
import br.com.sige.util.ConexaoException;

/**
 * Implementa o modelo padr�o DAO e acrescenta as demais fun��es epec�ficas do dom�nio (Titula��o)
 * @author titokenzo
 *
 */
public interface DAOTitulacao extends DAO<Titulacao> {
	
	/**
	 * Busca no BD pela chave do registro (codigo)
	 * @param codigo Chave da titulacao (codigo)
	 * @return Objeto encontrado no BD ou NULL
	 * @throws DAOException
	 * @throws ConexaoException
	 */
	public Titulacao get(int codigo)throws DAOException, ConexaoException;
	
}
