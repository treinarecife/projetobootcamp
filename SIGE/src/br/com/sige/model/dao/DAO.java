package br.com.sige.model.dao;

import java.util.List;

import br.com.sige.util.ConexaoException;


/**
 * Interface padr�o de dados
 * @author titokenzo
 *
 * @param <E> Na implementa��o deve subistituir pelo tipo espec�fico da Classe
 */
public interface DAO<E> {
	
	/**
	 * Armazena todos os dados do registro no BD
	 * @param obj Objeto contendo todos os dados, validados, para grava��o
	 * @throws DAOException
	 * @throws ConexaoException
	 */
	public void inserir(E obj)throws DAOException, ConexaoException;
	
	/**
	 * Efetua a altera��o em um registro do BD, pela chave fornecida (codigo, id, etc.)
	 * @param obj Objeto contendo todos os dados, validados, para grava��o
	 * @throws DAOException
	 * @throws ConexaoException
	 */
	public void alterar(E obj)throws DAOException, ConexaoException;
		
	/**
	 * Remove um registro do BD pela chave (codigo,id,etc.) 
	 * @param obj Objeto contendo a chave preenchida
	 * @throws DAOException
	 * @throws ConexaoException
	 */
	public void excluir(E obj)throws DAOException, ConexaoException;
	
	/**
	 * 
	 * @return Lista com todos os registros do BD
	 * @throws DAOException
	 * @throws ConexaoException
	 */
	public List<E> listar()throws DAOException, ConexaoException;

	/**
	 * Busca no BD pela chave do registro (codigo, id, etc.)
	 * @param obj Objeto contendo a chave preenchida (codigo, id, etc.)
	 * @return Objeto encontrado no BD ou NULL
	 * @throws DAOException
	 * @throws ConexaoException
	 */
	public E get(E obj)throws DAOException, ConexaoException;
	
}
