package br.com.sige.model.dao;

import java.util.List;
import br.com.sige.model.bean.Equipamento;
import br.com.sige.util.ConexaoException;

public interface DAOEquipamento {
	
	/**
	 * Faz uma consulta sem filtros, retornando todos os dados do BD
	 * @return Lista com todos os registros
	 * @throws ConexaoException 
	 * @throws DAOException
	 */
	public List<Equipamento> listar() throws ConexaoException, DAOException;
	
	/**
	 * Exclui registro de equipamento do BD
	 * @param Objeto equipamento a ser exclu�do
	 * @throws ConexaoException
	 * @throws DAOException 
	 */
	public void excluir(int codigo) throws ConexaoException, DAOException;
	
	/**
	 * Persiste uma atualiza��o do registro no BD
	 * @param Objeto equipamento com os dados validados
	 * @throws ConexaoException
	 * @throws DAOException 
	 */
	public void editar(Equipamento equipamento) throws ConexaoException, DAOException;
	
	/**
	 * Persiste um novo registro no BD
	 * @param Objeto equipamento com os dados validados
	 * @throws ConexaoException
	 * @throws DAOException 
	 */
	public void inserir(Equipamento equipamento) throws ConexaoException, DAOException;

}
