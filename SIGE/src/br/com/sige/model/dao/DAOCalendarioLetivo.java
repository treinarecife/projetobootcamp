package br.com.sige.model.dao;

import java.util.List;
import br.com.sige.model.bean.CalendarioLetivo;
import br.com.sige.util.ConexaoException;

public interface DAOCalendarioLetivo {
	
	/**
	 * Faz uma consulta sem filtros, retornando todos os dados do BD
	 * @return Lista com todos os registros
	 * @throws ConexaoException 
	 * @throws DAOException
	 */
	public List<CalendarioLetivo> listar() throws ConexaoException, DAOException;
	
	/**
	 * Exclui registro de disciplina do BD
	 * @param Objeto evento a ser exclu�do
	 * @throws ConexaoException
	 * @throws DAOException 
	 */
	public void excluir(int codigo) throws ConexaoException, DAOException;
	
	/**
	 * Persiste uma atualiza��o do registro no BD
	 * @param Objeto evento com os dados validados
	 * @throws ConexaoException
	 * @throws DAOException 
	 */
	public void editar(CalendarioLetivo evento) throws ConexaoException, DAOException;
	
	/**
	 * Persiste um novo registro no BD
	 * @param Objeto evento com os dados validados
	 * @throws ConexaoException
	 * @throws DAOException 
	 */
	public void inserir(CalendarioLetivo evento) throws ConexaoException, DAOException;

}
