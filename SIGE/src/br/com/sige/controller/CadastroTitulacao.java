package br.com.sige.controller;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import br.com.sige.model.dao.DAOException;
import br.com.sige.util.ConexaoException;
import br.com.sige.model.bean.Titulacao;
import br.com.sige.model.dao.DAOTitulacao;
import br.com.sige.model.dao.DAOTitulacaoImpl;

@SuppressWarnings("serial")
@Named
@RequestScoped
public class CadastroTitulacao implements Serializable {
	private Titulacao titulacao;
	private List<Titulacao> lista;
	private DAOTitulacao dao;

	public CadastroTitulacao() {
		this.titulacao = new Titulacao();
	}
	
	public List<Titulacao> getLista() {
		this.listar();
		return lista;
	}

	public void setLista(List<Titulacao> lista) {
		this.lista = lista;
	}
	
	public Titulacao getTitulacao() {
		return titulacao;
	}

	public void setTitulacao(Titulacao titulacao) {
		this.titulacao = titulacao;
	}
	
	public void listar() {
		try {
			this.dao = new DAOTitulacaoImpl();
			this.lista = dao.listar();
		} catch (DAOException erro) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("POBREMA NO DAO: " + erro.getMessage()));
		} catch (ConexaoException erro) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("POBREMA NA CONEX�O: " + erro.getMessage()));
		}
	}
	
	public String novo() {
		this.titulacao = new Titulacao();
		return "titulacao_editar";
	}
	
	public void excluir(int codigo) {
		try {
			this.dao = new DAOTitulacaoImpl();
			dao.excluir(new Titulacao(codigo));
		} catch (DAOException erro) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("POBREMA NO DAO: " + erro.getMessage()));
		} catch (ConexaoException erro) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("POBREMA NA CONEX�O: " + erro.getMessage()));
		}
	}
	
	public String editar(int codigo) {
		try {
			this.dao = new DAOTitulacaoImpl();
			this.titulacao = dao.get(new Titulacao(codigo));
			return "titulacao_editar";
		} catch (DAOException erro) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("POBREMA NO DAO: " + erro.getMessage()));
		} catch (ConexaoException erro) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("POBREMA NA CONEX�O: " + erro.getMessage()));
		}
		return null;
	}
	
	public String salvar() {
		if(this.titulacao.getCodigo()>0) {
			return alterar();
		}else {
			return incluir();
		}
	}

	public String incluir() {
		try {
			this.dao = new DAOTitulacaoImpl();
			dao.inserir(this.titulacao);
			return "titulacao";
		} catch (DAOException erro) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("POBREMA NO DAO: " + erro.getMessage()));
		} catch (ConexaoException erro) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("POBREMA NA CONEX�O: " + erro.getMessage()));
		}
		return null;
	}

	public String alterar() {
		try {
			this.dao = new DAOTitulacaoImpl();
			dao.alterar(this.titulacao);
			return "titulacao";
		} catch (DAOException erro) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("POBREMA NO DAO: " + erro.getMessage()));
		} catch (ConexaoException erro) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("POBREMA NA CONEX�O: " + erro.getMessage()));
		}
		return null;
	}
}
