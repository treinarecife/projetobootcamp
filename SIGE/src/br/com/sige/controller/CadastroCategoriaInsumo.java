package br.com.sige.controller;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import br.com.sige.model.bean.CategoriaInsumo;
import br.com.sige.model.dao.DAOCategoriaInsumo;
import br.com.sige.model.dao.DAOCategoriaInsumoImpl;
import br.com.sige.model.dao.DAOException;
import br.com.sige.util.ConexaoException;

@SuppressWarnings("serial")
@Named
@RequestScoped
public class CadastroCategoriaInsumo implements Serializable {
	private CategoriaInsumo categoriaInsumo;
	private List<CategoriaInsumo> lista;
	private DAOCategoriaInsumo dao;

	public CadastroCategoriaInsumo() {
		this.setCategoriaInsumo(new CategoriaInsumo());
		//this.dao = new DAOCategoriaInsumoImpl();
	}
	
	public CategoriaInsumo getCategoriaInsumo() {
		return categoriaInsumo;
	}

	public void setCategoriaInsumo(CategoriaInsumo categoriaInsumo) {
		this.categoriaInsumo = categoriaInsumo;
	}

	public String novo() {
		return null;
	}
	
	public List<CategoriaInsumo> getLista() {
		if(this.lista==null) {
			listar();
		}
		return lista;
	}

	public void setLista(List<CategoriaInsumo> lista) {
		this.lista = lista;
	}
	
	/**
	 * Salva tudo!
	 * @return
	 * @throws ConexaoException 
	 */
	public String salvar() throws ConexaoException {
		dao = new DAOCategoriaInsumoImpl(); 
		
		try {
			dao.inserir(categoriaInsumo);
			//
			
		} catch (DAOException erro) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Deu ruim no DAO: " + erro.getMessage()));
		} catch (ConexaoException erro) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Deu ruim no BD: " + erro.getMessage()));
			return null;
		}
		return "categoria_insumo?faces-redirect=true";
	}
	
	/**
	 * Lista
	 * @return
	 */
	public void listar(){
		
		try {
			dao = new DAOCategoriaInsumoImpl(); 
			//this.lista = new ArrayList<Fornecedor>();
			
			this.lista = dao.listar();
		} catch (DAOException erro) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("POBREMA NO DAO: " + erro.getMessage()));
		} catch (ConexaoException erro) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("POBREMA NA CONEXAO: " + erro.getMessage()));
		}
	}

	public void excluir(int codigo) {
		
		try {
			dao = new DAOCategoriaInsumoImpl(); 
			dao.excluir(codigo);
			this.lista = dao.listar();
		} catch (DAOException erro) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("POBREMA NO DAO: " + erro.getMessage()));
		} catch (ConexaoException erro) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("POBREMA NA CONEXO: " + erro.getMessage()));
		}
	}
		
	public String editar(int codigo) {
		try {
			this.dao = new DAOCategoriaInsumoImpl();
			dao.get(codigo);
		} catch (DAOException erro) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("POBREMA NO DAO: " + erro.getMessage()));
		} catch (ConexaoException erro) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("POBREMA NA CONEXO: " + erro.getMessage()));
		}
		return null;
	}		
	
}


