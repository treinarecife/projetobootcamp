package br.com.sige.controller;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import br.com.sige.model.bean.Funcionario;
import br.com.sige.model.dao.DAOException;
import br.com.sige.model.dao.DAOFuncionario;
import br.com.sige.model.dao.DAOFuncionarioImpl;
import br.com.sige.util.ConexaoException;

@SuppressWarnings("serial")
@Named
@SessionScoped
public class CadastroFuncionario implements Serializable {
	private Funcionario funcionario;
	private List<Funcionario> lista;
	private DAOFuncionario dao;

	public CadastroFuncionario() {
		this.funcionario = new Funcionario();
		listar();
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public List<Funcionario> getFuncionarios() {
		return lista;
	}

	public void setFuncionarios(List<Funcionario> funcionarios) {
		this.lista = funcionarios;
	}
	
	public void listar() {
		try {
			this.dao = new DAOFuncionarioImpl();
			this.lista = dao.listar();
		} catch (DAOException erro) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("PROBLEMA NO DAO: " + erro.getMessage()));
		} catch (ConexaoException erro) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("PROBLEMA NA CONEX�O: " + erro.getMessage()));
		}
	}

}
