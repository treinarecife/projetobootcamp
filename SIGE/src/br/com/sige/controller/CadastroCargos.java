package br.com.sige.controller;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import br.com.sige.model.bean.Cargos;
import br.com.sige.model.dao.DAOCargos;
import br.com.sige.model.dao.DAOCargosImpl;
import br.com.sige.model.dao.DAOException;
import br.com.sige.util.ConexaoException;

@SuppressWarnings("serial")
@Named
@RequestScoped
public class CadastroCargos implements Serializable {
	private Cargos cargos;
	private List<Cargos> lista;
	private DAOCargos dao;

	public CadastroCargos() throws ConexaoException {
		this.setCargos(new Cargos());
		this.dao = new DAOCargosImpl();
	}
	
	public Cargos getCargos() {
		return cargos;
	}

	public void setCargos(Cargos cargos) {
		this.cargos = cargos;
	}

	public String novo() {
		return null;
	}
	
	public List<Cargos> getLista() {
		if(this.lista==null) {
			listar();
		}
		return lista;
	}

	public void setLista(List<Cargos> lista) {
		this.lista = lista;
	}
	
	/**
	 * Salva tudo!
	 * @return
	 */
	public String salvar() {
		try {
			dao.inserir(this.cargos);
			
		} catch (DAOException erro) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Deu ruim no DAO: " + erro.getMessage()));
		} catch (ConexaoException erro) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Deu ruim no BD: " + erro.getMessage()));
		    return null;
		}
		return "cargos?faces-redirect=true";
	}
	
	/**
	 * Lista
	 * @return
	 */
	public void listar(){
		
		try {
			//this.lista = new ArrayList<Cargos>();
			this.dao = new DAOCargosImpl(); 
			this.lista = dao.listar();
		} catch (DAOException erro) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("POBREMA NO DAO: " + erro.getMessage()));
		} catch (ConexaoException erro) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("POBREMA NA CONEXAO: " + erro.getMessage()));
		}
	}

	public void excluir(int codigo) {
		
		try {
			dao = new DAOCargosImpl();
			dao.excluir(codigo);
			this.lista = dao.listar();
		} catch (DAOException erro) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("POBREMA NO DAO: " + erro.getMessage()));
		} catch (ConexaoException erro) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("POBREMA NA CONEXO: " + erro.getMessage()));
		}
	}
		
	public String editar(int codigo) {
		try {
			this.dao = new DAOCargosImpl();
			this.lista = dao.listar();
		} catch (DAOException erro) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("POBREMA NO DAO: " + erro.getMessage()));
		} catch (ConexaoException erro) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("POBREMA NA CONEXO: " + erro.getMessage()));
		}
		return null;
	}		
	
}


