package br.com.sige.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import br.com.sige.model.bean.Equipamento;
import br.com.sige.model.dao.DAOEquipamento;
import br.com.sige.model.dao.DAOEquipamentoImpl;
import br.com.sige.model.dao.DAOException;
import br.com.sige.util.ConexaoException;

@Named
@RequestScoped
@SuppressWarnings("serial")
public class CadastroEquipamento implements Serializable{
	
	private Equipamento equipamento;
	private List<Equipamento> listaEquipamentos;
	private DAOEquipamento dao;
	
	public CadastroEquipamento() {
		this.setEquipamento(new Equipamento());
	}

	public Equipamento getEquipamento() {
		return equipamento;
	}

	public void setEquipamento(Equipamento equipamento) {
		this.equipamento = equipamento;
	}

	public List<Equipamento> getListaEquipamentos() {
		return listaEquipamentos;
	}

	public void setListaEquipamentos(List<Equipamento> listaEquipamentos) {
		this.listaEquipamentos = listaEquipamentos;
	}

	public DAOEquipamento getDao() {
		return dao;
	}

	public void setDao(DAOEquipamento dao) {
		this.dao = dao;
	}

	public void listar() {
		this.listaEquipamentos = new ArrayList<Equipamento>();
		try {
			this.dao = new DAOEquipamentoImpl();
			this.listaEquipamentos = dao.listar();
		}catch(DAOException erroDAO) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Deu ruim no DAO " + erroDAO.getMessage()));
		}
		catch(ConexaoException erroConexao) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Deu ruim no BD " + erroConexao.getMessage()));
		}
	}
	
	public String excluir(int codigo) {
		try {
			this.dao = new DAOEquipamentoImpl();
			dao.excluir(codigo);
			return "equipamento-lista?faces-redirect=true";
		}catch(DAOException erroDAO) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Deu ruim no DAO " + erroDAO.getMessage()));
		}
		catch(ConexaoException erroConexao) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Deu ruim no BD " + erroConexao.getMessage()));
		}
		return null;
	}
	
	public void editar(Equipamento equipamento) {
		try {
			this.dao = new DAOEquipamentoImpl();
			dao.editar(equipamento);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Cadastro de " + equipamento.getNome() + "atualizado com sucesso!"));
		}
		catch(DAOException erroDAO) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Deu ruim no DAO " + erroDAO.getMessage()));
		}
		catch(ConexaoException erroConexao) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Deu ruim no BD " + erroConexao.getMessage()));
		}
		
	}
	
	public String inserir() {
		try {
			this.dao = new DAOEquipamentoImpl();
			dao.inserir(this.equipamento);
			return "equipamento-cadastro?faces-redirect=true";
		}
		catch(DAOException erroDAO) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Deu ruim no DAO " + erroDAO.getMessage()));
		}
		catch(ConexaoException erroConexao) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Deu ruim no BD " + erroConexao.getMessage()));
		}
		return null;
		
	}
	
	public String novo() {
		return "equipamento-cadastro";
	}
	

}
