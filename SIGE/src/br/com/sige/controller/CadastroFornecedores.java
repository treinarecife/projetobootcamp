package br.com.sige.controller;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import br.com.sige.model.bean.Fornecedores;
import br.com.sige.model.dao.DAOFornecedores;
import br.com.sige.model.dao.DAOFornecedoresImpl;
import br.com.sige.model.dao.DAOException;
import br.com.sige.util.ConexaoException;

@SuppressWarnings("serial")
@Named
@RequestScoped
public class CadastroFornecedores implements Serializable {
	private Fornecedores fornecedores;
	private List<Fornecedores> lista;
	private DAOFornecedores dao;

	public CadastroFornecedores() {
		this.setFornecedores(new Fornecedores());
		//this.dao = new DAOFornecedoresImpl();
	}
	
	public Fornecedores getFornecedores() {
		return fornecedores;
	}

	public void setFornecedores(Fornecedores fornecedores) {
		this.fornecedores = fornecedores;
	}

	public String novo() {
		return null;
	}
	
	public List<Fornecedores> getLista() {
		if(this.lista==null) {
			listar();
		}
		return lista;
	}

	public void setLista(List<Fornecedores> lista) {
		this.lista = lista;
	}
	
	/**
	 * Salva tudo!
	 * @return
	 */
	public String salvar() {
		
		
		try {
			dao = new DAOFornecedoresImpl(); 
			dao.inserir(fornecedores);
			//
			
		} catch (DAOException erro) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Deu ruim no DAO: " + erro.getMessage()));
		} catch (ConexaoException erro) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Deu ruim no BD: " + erro.getMessage()));
			return null;
		}
		return "fornecedores?faces-redirect=true";
	}
	
	/**
	 * Lista
	 * @return
	 */
	public void listar(){
		
		try {
			dao = new DAOFornecedoresImpl(); 
			//this.lista = new ArrayList<Fornecedor>();
			
			this.lista = dao.listar();
		} catch (DAOException erro) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("POBREMA NO DAO: " + erro.getMessage()));
		} catch (ConexaoException erro) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("POBREMA NA CONEXAO: " + erro.getMessage()));
		}
	}

	public void excluir(int codigo) {
		
		try {
			dao = new DAOFornecedoresImpl();
			dao.excluir(codigo);
			this.lista = dao.listar();
		} catch (DAOException erro) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("POBREMA NO DAO: " + erro.getMessage()));
		} catch (ConexaoException erro) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("POBREMA NA CONEXO: " + erro.getMessage()));
		}
	}
		
	public String editar(int codigo) {
		try {
			this.dao = new DAOFornecedoresImpl();
			dao.get(codigo);
		} catch (DAOException erro) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("POBREMA NO DAO: " + erro.getMessage()));
		} catch (ConexaoException erro) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("POBREMA NA CONEXO: " + erro.getMessage()));
		}
		return null;
	}		
	
}


