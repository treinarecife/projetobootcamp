package br.com.sige.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import br.com.sige.model.bean.Disciplina;
import br.com.sige.model.dao.DAODisciplina;
import br.com.sige.model.dao.DAODisciplinaImpl;
import br.com.sige.model.dao.DAOException;
import br.com.sige.util.ConexaoException;

@Named
@RequestScoped
@SuppressWarnings("serial")
public class CadastroDisciplina implements Serializable{
	
	private Disciplina disciplina;
	private List<Disciplina> listaDisciplinas;
	private DAODisciplina dao;

	public CadastroDisciplina() {
		this.setDisciplina(new Disciplina());
	}
	
	public Disciplina getDisciplina() {
		return disciplina;
	}

	public void setDisciplina(Disciplina disciplina) {
		this.disciplina = disciplina;
	}

	public List<Disciplina> getListaDisciplinas() {
		return listaDisciplinas;
	}

	public void setListaDisciplinas(List<Disciplina> listaDisciplinas) {
		this.listaDisciplinas = listaDisciplinas;
	}
	
	public void listar() {
		this.listaDisciplinas = new ArrayList<Disciplina>();
		try {
			this.dao = new DAODisciplinaImpl();
			this.listaDisciplinas = dao.listar();
		}catch(DAOException erroDAO) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Deu ruim no DAO " + erroDAO.getMessage()));
		}
		catch(ConexaoException erroConexao) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Deu ruim no BD " + erroConexao.getMessage()));
		}
	}
	
	public String excluir(int codigo) {
		try {
			this.dao = new DAODisciplinaImpl();
			dao.excluir(codigo);
			return "disciplina-lista?faces-redirect=true";
		}catch(DAOException erroDAO) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Deu ruim no DAO " + erroDAO.getMessage()));
		}
		catch(ConexaoException erroConexao) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Deu ruim no BD " + erroConexao.getMessage()));
		}
		return null;
	}
	
	public void editar(Disciplina disciplina) {
		try {
			this.dao = new DAODisciplinaImpl();
			dao.editar(disciplina);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Cadastro de " + disciplina.getNome() + "atualizado com sucesso!"));
		}
		catch(DAOException erroDAO) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Deu ruim no DAO " + erroDAO.getMessage()));
		}
		catch(ConexaoException erroConexao) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Deu ruim no BD " + erroConexao.getMessage()));
		}
		
	}
	
	public String inserir() {
		try {
			this.dao = new DAODisciplinaImpl();
			dao.inserir(this.disciplina);
			return "disciplina-cadastro?faces-redirect=true";
		}
		catch(DAOException erroDAO) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Deu ruim no DAO " + erroDAO.getMessage()));
		}
		catch(ConexaoException erroConexao) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Deu ruim no BD " + erroConexao.getMessage()));
		}
		return null;
		
	}
	
	public String novo() {
		return "disciplina-cadastro.xhtml";
	}
	

}
