package br.com.sige.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import br.com.sige.model.bean.Sala;
import br.com.sige.model.dao.DAOException;
import br.com.sige.model.dao.DAOSala;
import br.com.sige.model.dao.DAOSalaImpl;
import br.com.sige.util.ConexaoException;

@Named
@RequestScoped
@SuppressWarnings("serial")
public class CadastroSala implements Serializable{
	
	private Sala sala;
	private List<Sala> listaSalas;
	private DAOSala dao;
	
	public CadastroSala() {
		this.setSala(new Sala());
	}

	public Sala getSala() {
		return sala;
	}

	public void setSala(Sala sala) {
		this.sala = sala;
	}

	public List<Sala> getListaSalas() {
		return listaSalas;
	}

	public void setListaSalas(List<Sala> listaSalas) {
		this.listaSalas = listaSalas;
	}

	public DAOSala getDao() {
		return dao;
	}

	public void setDao(DAOSala dao) {
		this.dao = dao;
	}

	public void listar() {
		this.listaSalas = new ArrayList<Sala>();
		try {
			this.dao = new DAOSalaImpl();
			this.listaSalas = dao.listar();
		}catch(DAOException erroDAO) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Deu ruim no DAO " + erroDAO.getMessage()));
		}
		catch(ConexaoException erroConexao) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Deu ruim no BD " + erroConexao.getMessage()));
		}
	}
	
	public String excluir(int codigo) {
		try {
			this.dao = new DAOSalaImpl();
			dao.excluir(codigo);
			return "salas-lista?faces-redirect=true";
		}catch(DAOException erroDAO) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Deu ruim no DAO " + erroDAO.getMessage()));
		}
		catch(ConexaoException erroConexao) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Deu ruim no BD " + erroConexao.getMessage()));
		}
		return null;
	}
	
	public void editar(Sala sala) {
		try {
			this.dao = new DAOSalaImpl();
			dao.editar(sala);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Cadastro de " + sala.getNome() + "atualizado com sucesso!"));
		}
		catch(DAOException erroDAO) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Deu ruim no DAO " + erroDAO.getMessage()));
		}
		catch(ConexaoException erroConexao) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Deu ruim no BD " + erroConexao.getMessage()));
		}
		
	}
	
	public String inserir() {
		try {
			this.dao = new DAOSalaImpl();
			dao.inserir(this.sala);
			return "sala-cadastro?faces-redirect=true";
		}
		catch(DAOException erroDAO) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Deu ruim no DAO " + erroDAO.getMessage()));
		}
		catch(ConexaoException erroConexao) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Deu ruim no BD " + erroConexao.getMessage()));
		}
		return null;
		
	}
	
	public String novo() {
		return "sala-cadastro";
	}
	

}
