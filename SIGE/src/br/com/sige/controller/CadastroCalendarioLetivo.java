package br.com.sige.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import br.com.sige.model.bean.CalendarioLetivo;
import br.com.sige.model.dao.DAOCalendarioLetivo;
import br.com.sige.model.dao.DAOCalendarioLetivoImpl;
import br.com.sige.model.dao.DAOException;
import br.com.sige.util.ConexaoException;

@Named
@RequestScoped
@SuppressWarnings("serial")
public class CadastroCalendarioLetivo implements Serializable {
	
	private CalendarioLetivo evento;
	private List<CalendarioLetivo> listaEventos;
	private DAOCalendarioLetivo dao;
	
	public CalendarioLetivo getEvento() {
		return evento;
	}
	public void setEvento(CalendarioLetivo evento) {
		this.evento = evento;
	}
	public List<CalendarioLetivo> getListaEventos() {
		return listaEventos;
	}
	public void setListaEventos(List<CalendarioLetivo> listaEventos) {
		this.listaEventos = listaEventos;
	}
	public DAOCalendarioLetivo getDao() {
		return dao;
	}
	public void setDao(DAOCalendarioLetivo dao) {
		this.dao = dao;
	}
	
	public void listar() {
		this.listaEventos = new ArrayList<CalendarioLetivo>();
		try {
			this.dao = new DAOCalendarioLetivoImpl();
			this.listaEventos = dao.listar();
		}catch(DAOException erroDAO) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Deu ruim no DAO " + erroDAO.getMessage()));
		}
		catch(ConexaoException erroConexao) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Deu ruim no BD " + erroConexao.getMessage()));
		}
	}
	
	public String excluir(int codigo) {
		try {
			this.dao = new DAOCalendarioLetivoImpl();
			dao.excluir(codigo);
			return "calendarioletivo-lista?faces-redirect=true";
		}catch(DAOException erroDAO) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Deu ruim no DAO " + erroDAO.getMessage()));
		}
		catch(ConexaoException erroConexao) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Deu ruim no BD " + erroConexao.getMessage()));
		}
		return null;
	}
	
	public void editar(CalendarioLetivo evento) {
		try {
			this.dao = new DAOCalendarioLetivoImpl();
			dao.editar(evento);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Cadastro de " + evento.getEvento() + "atualizado com sucesso!"));
		}
		catch(DAOException erroDAO) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Deu ruim no DAO " + erroDAO.getMessage()));
		}
		catch(ConexaoException erroConexao) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Deu ruim no BD " + erroConexao.getMessage()));
		}
		
	}
	
	public String inserir() {
		try {
			this.dao = new DAOCalendarioLetivoImpl();
			dao.inserir(this.evento);
			return "calendarioletivo-cadastro?faces-redirect=true";
		}
		catch(DAOException erroDAO) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Deu ruim no DAO " + erroDAO.getMessage()));
		}
		catch(ConexaoException erroConexao) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Deu ruim no BD " + erroConexao.getMessage()));
		}
		return null;
		
	}
	
	public String novo() {
		return "calendarioletivo-cadastro.xhtml";
	}
	

}
