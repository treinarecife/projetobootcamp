package br.com.sige.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import br.com.sige.model.bean.Professor;
import br.com.sige.model.dao.DAOProfessor;
import br.com.sige.model.dao.DAOException;
import br.com.sige.model.dao.DAOProfessorImpl;
import br.com.sige.util.ConexaoException;

@SuppressWarnings("serial")
@Named
@RequestScoped
public class CadastroProfessor implements Serializable {
	private Professor professor;
	private List<Professor> lista;
	private DAOProfessor dao;
	
	public CadastroProfessor() {
		this.setProfessor(new Professor());
	}

	public Professor getProfessor() {
		return professor;
	}

	public void setProfessor(Professor professor) {
		this.professor = professor;
	}

	public List<Professor> getLista() {
		this.listar();
		return lista;
	}

	public void setLista(List<Professor> lista) {
		this.lista = lista;
	}

	public DAOProfessor getDao() {
		return dao;
	}

	public void setDao(DAOProfessor dao) {
		this.dao = dao;
	}
	
	@PostConstruct
	public void listar() {
		try {
			this.dao = new DAOProfessorImpl();
			this.lista = dao.listar();
		} catch(DAOException erro) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Ocorreu um problema no DAO professor: "+erro.getMessage()));
		} catch(ConexaoException erro) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Ocorreu um problema na conex�o: "+erro.getMessage()));
		}
	}
	
	public void editar() {
		
	}
	
	public void excluir(String matricula) {
		
	}

	
}
